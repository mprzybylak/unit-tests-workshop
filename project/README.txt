UNIT TESTS WORKSHOP - README

= Wstep =

Plik  readme zawiera  roznego  rodzaju  informacje dotyczace  projektu,  ktore z 
jakiegos powodu  powinny zostac zapisane, np. rozne  pomysly, linki do ciekawych
artykulow, etc.

= JUnit todo ==

* anotacja Rule i tematy pochodne http://cwd.dhemery.com/2010/12/junit-rules/
* rozne artykuly o testowaniu zwiazane z junit https://blogs.oracle.com/jacobc/ (m.in. o Therioes)
* Junit theories http://blog.schauderhaft.de/2010/02/07/junit-theories/ , http://blog.schauderhaft.de/2010/01/31/new-feature-of-junit-theories/
* Przeniesc parametryzowane testy do osobnego projektu mavenowego

== Fest todo ==

Custom cell editor - http://docs.codehaus.org/display/FEST/Custom+Cell+Editors
Custom cell renderer - http://docs.codehaus.org/display/FEST/Custom+Cell+Renderers

== Hamcrest todo ==

== Mockito todo ==

* podzielic testy miedzy klasy

= Pomysly =

* Mechanizmy mierzenia pokrycia kodu testami
* Mechanizmy continious integration dotyczace testow jednostkowych
* Mechanizmy do narzedzi typu maven zwiazane z testami jednostkowymi