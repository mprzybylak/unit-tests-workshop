package com.przybylak.workshops.units.fest.basic;

import org.fest.swing.core.Robot;
import org.fest.swing.core.Settings;
import org.fest.swing.edt.FailOnThreadViolationRepaintManager;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RobotConfiguration 
{
	private FrameFixture frameFixture;
	
	@BeforeClass
	public static void setUpOnce()
	{
		FailOnThreadViolationRepaintManager.install();
	}
	
	@Before
	public void setUp()
	{
		SimpleWindow window = GuiActionRunner.execute(new GuiQuery<SimpleWindow>() 
		{
			@Override
			protected SimpleWindow executeInEDT() throws Throwable 
			{
				return new SimpleWindow();
			}
		});
		
		frameFixture = new FrameFixture(window);
		frameFixture.show();
	}
	
	@After
	public void tearDown()
	{
		frameFixture.cleanUp();
	}
	
	@Test
	public void shouldConfigureRobot()
	{
		Robot robot = frameFixture.robot;
		Settings settings = robot.settings();
		
		// which components should participate in component lookup
		settings.componentLookupScope(); // get
		// settings.componentLookupScope(ComponentLookupScope.SHOWING_ONLY); // set - looks the same for every settings
		
		// delay between events
		settings.delayBetweenEvents();
		
		// delay between mouse click, and mouse drag move
		settings.dragDelay();
		
		// delay between mouse drag move and release of button
		settings.dropDelay();
		
		// delay before checking for idle
		settings.eventPostingDelay();
		
		// time to waiting for an idle AWT event queue
		settings.idleTimeout();
		
		// time to waiting for component to be visible
		settings.timeoutToBeVisible();
		
		// time to find popup (fail on timeout)
		settings.timeoutToFindPopup();
		
		// time to find (fail on timeout)
		settings.timeoutToFindSubMenu();
	}
}
