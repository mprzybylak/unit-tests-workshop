package com.przybylak.workshops.units.fest.basic;

import java.awt.Color;

import org.fest.swing.core.ComponentLookupScope;
import org.fest.swing.edt.FailOnThreadViolationRepaintManager;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.fixture.JButtonFixture;
import org.fest.swing.fixture.JLabelFixture;
import org.fest.util.VisibleForTesting;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ColorAndFonts 
{
	private FrameFixture frameFixture;
	private JLabelFixture label;
	private JButtonFixture redButton;
	private JButtonFixture yellowButton;
	private JButtonFixture greenButton;
	
	@BeforeClass
	public static void setUpOnce()
	{
		FailOnThreadViolationRepaintManager.install();
	}
	
	@Before
	public void setUp()
	{
		ColorWindow window = GuiActionRunner.execute(new GuiQuery<ColorWindow>() {

			@Override
			protected ColorWindow executeInEDT() throws Throwable {
				return new ColorWindow();
			}
		});
		
		frameFixture = new FrameFixture(window);
		frameFixture.show();
		label = frameFixture.label("label");
		redButton = frameFixture.button("red");
		yellowButton = frameFixture.button("yellow");
		greenButton = frameFixture.button("green");
	}
	
	@After
	public void tearDown()
	{
		frameFixture.cleanUp();
	}
	
	@Test
	public void shouldTestColors()
	{
		redButton.click();
		label.foreground().requireEqualTo(Color.RED);
		
		yellowButton.click();
		label.foreground().requireEqualTo(Color.YELLOW);
		
		greenButton.click();
		label.foreground().requireEqualTo(Color.GREEN);
	}
	
	@Test
	public void shouldTestFont()
	{
		redButton.click();
		label.font().requireBold();
		
		yellowButton.click();
		label.font().requireItalic();
	}
}
