package com.przybylak.workshops.units.fest.basic;

import org.fest.swing.edt.FailOnThreadViolationRepaintManager;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GettingStarted 
{
	private FrameFixture frameFixture;
	
	@BeforeClass
	public static void setUpOnce()
	{
		FailOnThreadViolationRepaintManager.install();
	}
	
	@Before
	public void setUp()
	{
		SimpleWindow window = GuiActionRunner.execute(new GuiQuery<SimpleWindow>() 
		{
			@Override
			protected SimpleWindow executeInEDT() throws Throwable 
			{
				return new SimpleWindow();
			}
		});
		
		frameFixture = new FrameFixture(window);
		frameFixture.show();
	}
	
	@After
	public void tearDown()
	{
		frameFixture.cleanUp();
	}
	
	@Test
	public void shouldCopyTextFromFieldToLabel()
	{
		frameFixture.textBox().enterText("Lorem ipsum");
		frameFixture.button().click();
		frameFixture.label().requireText("Lorem ipsum");
	}
}
