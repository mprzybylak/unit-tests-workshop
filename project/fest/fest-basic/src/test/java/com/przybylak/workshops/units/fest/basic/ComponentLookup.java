package com.przybylak.workshops.units.fest.basic;

import static org.fest.swing.core.matcher.JButtonMatcher.withName;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import javax.swing.JButton;

import org.fest.swing.core.BasicRobot;
import org.fest.swing.core.ComponentLookupScope;
import org.fest.swing.core.GenericTypeMatcher;
import org.fest.swing.core.Robot;
import org.fest.swing.core.matcher.DialogMatcher;
import org.fest.swing.edt.FailOnThreadViolationRepaintManager;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.exception.ComponentLookupException;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.fixture.JButtonFixture;
import org.fest.swing.fixture.JLabelFixture;
import org.junit.BeforeClass;
import org.junit.Test;

public class ComponentLookup {
	
	@BeforeClass
	public static void setUpOnce()
	{
		FailOnThreadViolationRepaintManager.install();
	}
	
	@Test
	public void shouldMatchWithWindowFixture()
	{
		FrameFixture colorFixture = createColorWindowFixture();

		// try to match label
		JLabelFixture label = colorFixture.label();
		
		// try to match button with name
		JButtonFixture button = colorFixture.button("green");
		
		colorFixture.cleanUp();
	}
	
	@Test
	public void shouldMatchButtonWithJButtonMatcher()
	{
		FrameFixture colorWindowFixture = createColorWindowFixture();
		
		final JButtonFixture button = colorWindowFixture.button(withName("green").andText("Green").andShowing());
		
		String name = getButtonName(button);
		String text = getButtonText(button);
		Boolean visible = getButtonVisiblity(button);
		
		assertThat(name, is(equalTo("green")));
		assertThat(text, is(equalTo("Green")));
		assertThat(visible, is(equalTo(true)));
		
		colorWindowFixture.cleanUp();
	}
	
	@Test
	public void shouldMatchDialogWithJDialogMatcher()
	{
		DialogShowWindow window = GuiActionRunner.execute(new GuiQuery<DialogShowWindow>() 
		{
			@Override
			protected DialogShowWindow executeInEDT() throws Throwable 
			{
				return new DialogShowWindow();
			}
		});

		FrameFixture dialogShowWindowFixture = new FrameFixture(window);
		dialogShowWindowFixture.show();
		
		dialogShowWindowFixture.button().click();
		
		final DialogFixture dialog = dialogShowWindowFixture.dialog(DialogMatcher.withTitle("Title").andShowing());
		
		String title = GuiActionRunner.execute(new GuiQuery<String>() {

			@Override
			protected String executeInEDT() throws Throwable {
				return dialog.target.getTitle();
			}
		});
		
		assertThat(title, is(equalTo("Title")));
		
		dialogShowWindowFixture.cleanUp();
	}
	
	/*
	 * There are more common matchers:
	 * FrameMatcher
	 * JLabelMatcher
	 * JTextComponentMatcher
	 */
	
	@Test
	public void shouldFindButtonWithGenericMatcher()
	{
		FrameFixture colorWindowFixture = createColorWindowFixture();
		
		final JButtonFixture button = colorWindowFixture.button(new GenericTypeMatcher<JButton>(JButton.class) {
			@Override
			protected boolean isMatching(JButton button) {
				if(button.getName().equals("green"))
					return true;
				return false;
			}
		});
		
		String name = GuiActionRunner.execute(new GuiQuery<String>() {

			@Override
			protected String executeInEDT() throws Throwable {
				return button.target.getName();
			}
		});
		
		assertThat(name, is(equalTo("green")));
		
		colorWindowFixture.cleanUp();
	}
	
	@Test
	public void lookupSettings()
	{
		FrameFixture hideAndSeekFixture = createHideAndSeekFixture();
		
		try
		{
			// default behavior is to not seek for invisible objects - so exception will be thrown
			hideAndSeekFixture.button("invisible");
		}
		catch(ComponentLookupException e) { ; }
		
		// we can turn on lookup for invisible components 
		hideAndSeekFixture.robot.settings().componentLookupScope(ComponentLookupScope.ALL);
		
		final JButtonFixture button = hideAndSeekFixture.button("Invisible");
		
		String name = GuiActionRunner.execute(new GuiQuery<String>() 
		{
			@Override
			protected String executeInEDT() throws Throwable 
			{
				return button.target.getName();
			}
		});
		
		assertThat(name, is(equalTo("Invisible")));
		
		hideAndSeekFixture.cleanUp();
	}
	
	private FrameFixture createHideAndSeekFixture() 
	{
		HideAndSeek window = GuiActionRunner.execute(new GuiQuery<HideAndSeek>() 
		{
			@Override
			protected HideAndSeek executeInEDT() throws Throwable 
			{
				return new HideAndSeek();
			}
		});
		
		FrameFixture fixture = new FrameFixture(window);
		fixture.show();
		return fixture;
	}

	private Boolean getButtonVisiblity(final JButtonFixture button) {
		Boolean visible = GuiActionRunner.execute(new GuiQuery<Boolean>() 
		{
			@Override
			protected Boolean executeInEDT() throws Throwable 
			{
				return button.target.isVisible();
			}
		});
		return visible;
	}

	private String getButtonText(final JButtonFixture button) {
		String text = GuiActionRunner.execute(new GuiQuery<String>() 
		{
			@Override
			protected String executeInEDT() throws Throwable 
			{
				return button.target.getText();
			}
		});
		return text;
	}

	private String getButtonName(final JButtonFixture button) {
		String name = GuiActionRunner.execute(new GuiQuery<String>() 
		{
			@Override
			protected String executeInEDT() throws Throwable 
			{
				return button.target.getName();
			}
		});
		return name;
	}

	private FrameFixture createColorWindowFixture() {
		ColorWindow window = GuiActionRunner.execute(new GuiQuery<ColorWindow>() {

			@Override
			protected ColorWindow executeInEDT() throws Throwable {
				return new ColorWindow();
			}
		});
		
		FrameFixture colorWindowFixture = new FrameFixture(window);
		colorWindowFixture.show();
		return colorWindowFixture;
	}
}




