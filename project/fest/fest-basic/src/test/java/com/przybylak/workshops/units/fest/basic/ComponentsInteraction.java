package com.przybylak.workshops.units.fest.basic;

import org.fest.swing.core.MouseButton;
import org.fest.swing.core.MouseClickInfo;
import org.fest.swing.core.matcher.JButtonMatcher;
import org.fest.swing.edt.FailOnThreadViolationRepaintManager;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.fixture.JButtonFixture;
import org.fest.swing.fixture.JCheckBoxFixture;
import org.fest.swing.fixture.JComboBoxFixture;
import org.fest.swing.fixture.JLabelFixture;
import org.fest.swing.fixture.JListFixture;
import org.fest.swing.fixture.JProgressBarFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ComponentsInteraction 
{
	FrameFixture frameFixture = null;
	
	@BeforeClass
	public static void setUpOnce()
	{
		FailOnThreadViolationRepaintManager.install();
	}
	
	@Before
	public void setUp()
	{
		ComponentsWidow window = GuiActionRunner.execute(new GuiQuery<ComponentsWidow>() 
		{
			@Override
			protected ComponentsWidow executeInEDT() throws Throwable 
			{
				return new ComponentsWidow();
			}
		});
		
		frameFixture = new FrameFixture(window);
		frameFixture.show();
	}
	
	@After
	public void tearDown()
	{
		frameFixture.cleanUp();
	}
	
	@Test
	public void mouseInputInterface()
	{
		JButtonFixture button = frameFixture.button(JButtonMatcher.withText("click me"));
		
		// regular
		button.click();
		button.rightClick();

		// double click
		button.doubleClick();
		
		// which button
		button.click(MouseButton.LEFT_BUTTON);
		button.click(MouseButton.MIDDLE_BUTTON);
		button.click(MouseButton.RIGHT_BUTTON);
		
		// advance setup
		MouseClickInfo clickInfo = MouseClickInfo.leftButton().times(2).rightButton();
		button.click(clickInfo);
	}
	
	@Test
	public void buttonInteractions()
	{
		JButtonFixture button = frameFixture.button(JButtonMatcher.withText("click me"));
		
		// FOCUSING
		button.focus();
		
		// REQUIREMENTS
		button.requireEnabled();
		button.requireFocused();
		button.requireText("click me");
		button.requireVisible();
	}
	
	@Test
	public void checkBoxInteractions()
	{
		JCheckBoxFixture checkbox = frameFixture.checkBox();
		
		// CHECK
		checkbox.check();
		checkbox.uncheck();
		
		// CLICK
		checkbox.click(); // the same methods as in button
		
		checkbox.requireSelected();
	}
	
	@Test
	public void comboBoxInteractions()
	{
		JComboBoxFixture combo = frameFixture.comboBox();

		// clear selection
		combo.clearSelection();

		// selection
		combo.selectItem(0);
		combo.selectItem("B");
		
		// text
		combo.enterText("test");
		combo.selectAllText();

		combo.requireSelection("B");
	}
	
	@Test
	public void labelInteractions()
	{
		JLabelFixture label = frameFixture.label();
		
		label.requireText("Lorem ipsum");
	}
	
	@Test
	public void listInteractions()
	{
		JListFixture list = frameFixture.list();

		// clear selection
		list.clearSelection();
		
		// selection
		list.clickItem("1");
		list.selectItem("2");
		
		// drag&drop
		list.drag(0);
		list.drop(2);
		
		list.requireSelectedItems("3");
	}
	
	@Test
	public void progressbarInteraction()
	{
		JProgressBarFixture progressbar = frameFixture.progressBar();
		
		progressbar.requireValue(0);
	}
}
