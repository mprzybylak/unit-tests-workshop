package com.przybylak.workshops.units.fest.basic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import javax.swing.JLabel;

import org.fest.swing.edt.FailOnThreadViolationRepaintManager;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.edt.GuiTask;
import org.fest.swing.exception.EdtViolationException;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.fixture.JLabelFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AccessingComponentsInEDT 
{
	private FrameFixture frameFixture;
	
	@BeforeClass
	public static void setUpOnce()
	{
		FailOnThreadViolationRepaintManager.install();
	}
	
	@Before
	public void setUp()
	{
		SimpleWindow window = GuiActionRunner.execute(new GuiQuery<SimpleWindow>() 
		{
			@Override
			protected SimpleWindow executeInEDT() throws Throwable 
			{
				return new SimpleWindow();
			}
		});
		
		frameFixture = new FrameFixture(window);
		frameFixture.show();
	}
	
	@After
	public void tearDown()
	{
		frameFixture.cleanUp();
	}
	
	@Test
	public void shouldAccessComponentByQuery()
	{
		// given
		final JLabelFixture label = frameFixture.label();
		
		// when
		String text = GuiActionRunner.execute(new GuiQuery<String>() {

			@Override
			protected String executeInEDT() throws Throwable {
				return label.text();
			}
		});
		
		// then
		assertThat(text, is(equalTo(" ")));
	}
	
	@Test
	public void shouldAccessComponentByTask()
	{
		// given
		final JLabelFixture label = frameFixture.label();
		
		// when
		GuiActionRunner.execute(new GuiTask() 
		{
			@Override
			protected void executeInEDT() throws Throwable 
			{
				label.target.setText("Text");
			}
		});
		
		// then
		label.requireText("Text");
	}
	
	@Test(expected=EdtViolationException.class)
	public void shouldViolateEDTAccess()
	{
		// given
		JLabel label = frameFixture.label().target;
		
		// when
		label.setText("Text");
		
		// then
		fail();
		
	}
}
