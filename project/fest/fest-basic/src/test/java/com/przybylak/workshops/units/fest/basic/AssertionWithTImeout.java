package com.przybylak.workshops.units.fest.basic;

import org.fest.swing.core.matcher.JButtonMatcher;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.fixture.JButtonFixture;
import org.fest.swing.fixture.WindowFixture;
import org.fest.swing.timing.Condition;
import org.fest.swing.timing.Pause;
import org.fest.swing.timing.Timeout;
import org.junit.Test;

public class AssertionWithTImeout 
{
	@Test
	public void shouldPerformTestWithTimeout()
	{
		// given
		SimpleWindow simpleWindow = GuiActionRunner.execute(new GuiQuery<SimpleWindow>() 
		{
			@Override
			protected SimpleWindow executeInEDT() throws Throwable 
			{
				return new SimpleWindow();
			}
		});
		
		FrameFixture window = new FrameFixture(simpleWindow);
		window.show();
		
		final JButtonFixture buttonFixture = window.button();
		
		// when
		Pause.pause(new Condition("Button click") 
		{
			
			@Override
			public boolean test() 
			{
				return buttonFixture.target.isEnabled();
			}
		}, Timeout.timeout(10000));
	}
}
