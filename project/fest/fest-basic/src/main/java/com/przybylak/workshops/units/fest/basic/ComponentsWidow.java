package com.przybylak.workshops.units.fest.basic;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class ComponentsWidow extends JFrame 
{

	private static final long serialVersionUID = -6741166191987714213L;

	private JButton button;
	private JCheckBox checkBox;
	private JComboBox comboBox;
	private JLabel label;

	private JList list;

	private JProgressBar progressbar;
	
	public ComponentsWidow() 
	{
		// layout
		BoxLayout box = new BoxLayout(getContentPane(), BoxLayout.Y_AXIS);
		setLayout(box);
		
		// components initialization
		button = new JButton("click me");
		button.setAlignmentX(0.5F);
		
		checkBox = new JCheckBox("Checkbox");
		checkBox.setAlignmentX(0.5F);
		
		comboBox = new JComboBox(new String[]{"A", "B", "C"});
		comboBox.setEditable(true);
		comboBox.setAlignmentX(0.5F);
		
		label = new JLabel("Lorem ipsum");
		label.setAlignmentX(0.5F);
		
		list = new JList(new String[]{"1", "2", "3"});
		list.setAlignmentX(0.5F);
		
		progressbar = new JProgressBar(0, 100);
		progressbar.setAlignmentX(0.5F);
		
		// adding component to window
		add(button);
		add(checkBox);
		add(comboBox);
		add(label);
		add(list);
		add(progressbar);
		
		pack();
	}
	
	public static void main(String[] args) 
	{
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() 
			{
				ComponentsWidow window = new ComponentsWidow();
				window.setVisible(true);
			}
		});
	}
}
