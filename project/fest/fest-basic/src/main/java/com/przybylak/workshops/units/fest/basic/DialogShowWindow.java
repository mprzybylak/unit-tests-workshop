package com.przybylak.workshops.units.fest.basic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DialogShowWindow extends JFrame
{
	private static final long serialVersionUID = -1094849666577556270L;

	public DialogShowWindow() 
	{
		JButton button = new JButton("Show dialog");
		getContentPane().add(button);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(DialogShowWindow.this, "Message", "Title", JOptionPane.INFORMATION_MESSAGE);
			}
		});
	}
}
