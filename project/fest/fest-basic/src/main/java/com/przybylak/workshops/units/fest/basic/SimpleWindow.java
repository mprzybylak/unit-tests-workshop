package com.przybylak.workshops.units.fest.basic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class SimpleWindow extends JFrame
{
	private static final long serialVersionUID = -7670754464545334124L;
	
	private JTextField textField;
	private JButton button;
	private JLabel label;
	
	public SimpleWindow() 
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

		textField = new JTextField();
		button = new JButton("Copy");
		label = new JLabel(" ");
		
		getContentPane().add(textField);
		getContentPane().add(button);
		getContentPane().add(label);
		
		button.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				label.setText(textField.getText());
			}
		});
		
		pack();
	}
	
	public static void main(String[] args) 
	{
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				SimpleWindow w = new SimpleWindow();
				w.setVisible(true);
			}
		});
		
	}
}
