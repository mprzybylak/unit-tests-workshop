package com.przybylak.workshops.units.fest.basic;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

public class HideAndSeek extends JFrame 
{
	public HideAndSeek() 
	{
		BoxLayout mainLayout = new BoxLayout(getContentPane(), BoxLayout.X_AXIS);
		getContentPane().setLayout(mainLayout);
		
		JButton visible = new JButton("Visible");
		visible.setName("Visible");
		
		JButton invisible = new JButton("Invisible");
		invisible.setName("Invisible");
		
		invisible.setVisible(false);
		
		getContentPane().add(visible);
		getContentPane().add(invisible);
	}
}
