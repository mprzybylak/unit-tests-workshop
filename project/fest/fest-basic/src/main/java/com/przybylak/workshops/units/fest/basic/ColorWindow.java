package com.przybylak.workshops.units.fest.basic;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

enum FontWeight
{
	BOLD {@Override int getWeight() {return Font.BOLD;}},
	ITALIC{@Override int getWeight() {return Font.ITALIC;}},
	PLAIN{@Override int getWeight() {return Font.PLAIN;}};
	
	abstract int getWeight();
};

public class ColorWindow extends JFrame
{
	private static final long serialVersionUID = 1406692660516697228L;
	
	public ColorWindow() 
	{
		BoxLayout mainLayout = new BoxLayout(getContentPane(), BoxLayout.X_AXIS);
		getContentPane().setLayout(mainLayout);
		
		JPanel left = new JPanel();
		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
		
		JButton green = new JButton("Green");
		JButton yellow = new JButton("Yellow");
		JButton red = new JButton("Red");
		
		green.setName("green");
		yellow.setName("yellow");
		red.setName("red");
		
		left.add(green);
		left.add(yellow);
		left.add(red);
		
		
		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		
		final JLabel label = new JLabel("Lorem ipsum dolot");
		label.setName("label");
		
		right.add(label);
		
		getContentPane().add(left);
		getContentPane().add(right);
		
		pack();

		green.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				label.setFont(createFontWithGivenWeightFromLabel(label, FontWeight.PLAIN));
				label.setForeground(Color.GREEN);
			}
		});
		
		yellow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				label.setFont(createFontWithGivenWeightFromLabel(label, FontWeight.ITALIC));
				label.setForeground(Color.YELLOW);
			}
		});
		red.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				label.setFont(createFontWithGivenWeightFromLabel(label, FontWeight.BOLD));
				label.setForeground(Color.RED);
			}
		});
	}
	
	private Font createFontWithGivenWeightFromLabel(JLabel label, FontWeight weight)
	{
		return new Font(label.getFont().getName(), weight.getWeight(), label.getFont().getSize());
	}
	
	public static void main(String[] args) 
	{
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				ColorWindow w = new ColorWindow();
				w.setVisible(true);
			}
		});
	}
}
