package com.przybylak.workshops.units.mockito.basic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.timeout;
import static org.hamcrest.CoreMatchers.not;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.exceptions.verification.SmartNullPointerException;
import org.mockito.internal.stubbing.answers.CallsRealMethods;
import org.mockito.internal.util.MockUtil;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.listeners.InvocationListener;
import org.mockito.listeners.MethodInvocationReport;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;


class A
{
	private B b;
}

class B 
{
	
}

@RunWith(MockitoJUnitRunner.class) // we need this runner for mockito annotations
public class MockitoTutorial 
{
	private class Bar 
	{
		public String getBarString()
		{
			return "Bar";
		}
	}
	
	private class Foo
	{
		public Bar getBar()
		{
			return new Bar();
		}
	}
	
	private class Company
	{
		public void hire(Person p)
		{
			;
		}
	}
	
	private class Person
	{
		String name;
		
		Person(String name)
		{
			this.name = name;
		}
	}
	
	/**
	 * We can create mock by annotation but we have to use Mockito Runner or
	 * we have to execute {@link MockitoAnnotations.initMocks(testClass)} somewhere in code
	 * manualy. Besides mock we can create captors and spies
	 */
	@Mock private List list;
	@Captor private ArgumentCaptor<List> listCaptor;
	@Spy private ArrayList<Integer> spy;
	
	
	/**
	 * Mock injection - we can create object and inject mocks created with @Mock annotation (or @Spy)
	 */
	@Mock private B b;
	@InjectMocks private A a;
	
	/**
	 * Spy objects are something like partial mocks - they will execute real methods of objects,
	 * but we can se stub selected methods.
	 */
	@Test
	public void spyingObjects()
	{
		// start spying
		ArrayList<Integer> spyList = spy(new ArrayList<Integer>());
		
		// we can stub methods
		when(spyList.size()).thenReturn(100);
		
		// we have to use doReturn/ doAnswer with NON-voids, in special cases, such as partial mocks
		
		// when(spyList.get(1)).thenReturn(1); // error - spyList.get(1) will execute real method, so it will throw exception	
		
		doReturn(1).when(spyList).get(1);
		doAnswer(new Answer<Integer>() {
			@Override
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				return 1;
			}
		}).when(spyList).add(1);
		
		spyList.size();
		spyList.get(1);
		
		// we can verify spy objects
		verify(spyList).size(); // real methods verify
		verify(spyList).get(1); // stub methods verify
	}
	
	@Test
	public void defaultReturnValues()
	{
		
		// default behavior
		ArrayList<Integer> regularMock = mock(ArrayList.class);
		ArrayList<Integer> regularMock2 = mock(ArrayList.class, Mockito.RETURNS_DEFAULTS);
		
		Integer integer = regularMock.get(1);
		Integer integer2 = regularMock2.get(1);
		
		assertThat(integer, is(nullValue()));
		assertThat(integer2, is(nullValue()));
		
		// smart null
		Foo smartNullMock = mock (Foo.class, Mockito.RETURNS_SMART_NULLS);

		Bar bar = smartNullMock.getBar();
		
		try
		{
			bar.getBarString();
			fail();
		}
		catch(SmartNullPointerException e)
		{
			;
		}
		
		// returns mock
		Foo mockMock = mock(Foo.class, Mockito.RETURNS_MOCKS);
		
		Bar barMock = mockMock.getBar();
		boolean isMock = new MockUtil().isMock(barMock);
		assertThat(isMock, is(true));
		
		// deep stubs
		Foo deepStubsMock = mock(Foo.class, Mockito.RETURNS_DEEP_STUBS);
		when(deepStubsMock.getBar().toString()).thenReturn("Deep Stub");
		assertThat(deepStubsMock.getBar().toString(), is(equalTo("Deep Stub")));
		
		// real methods
		ArrayList<Integer> callRealMethods = mock(ArrayList.class, Mockito.CALLS_REAL_METHODS);
		
		try
		{
			callRealMethods.get(1);
			fail();
		}
		catch(IndexOutOfBoundsException e)
		{
			;
		}
	}
	
	@Test
	public void argumetCaptor()
	{
		Company c = mock(Company.class);
		Person p = new Person("John");
		
		ArgumentCaptor<Person> captor = ArgumentCaptor.forClass(Person.class);
		
		c.hire(p);
		
		verify(c).hire(captor.capture());
		assertThat(captor.getValue().name, is(equalTo("John")));
	}
	
	@Test
	public void mockReseting()
	{
		ArrayList<Integer> mock = mock(ArrayList.class);
		when(mock.size()).thenReturn(10);
		
		assertThat(mock.size(), is(equalTo(10)));
		reset(mock);
		assertThat(mock.size(), is(not(equalTo(10))));
 	}
	
	@Test
	public void serializableMocks()
	{
		// serializable mock
		ArrayList<Integer> serializableMock = mock(ArrayList.class, Mockito.withSettings().serializable());
		
		// seriazliable spying
		ArrayList<Integer> list = new ArrayList<Integer>();
		ArrayList<Integer> serializableSpyedObject = mock(ArrayList.class, Mockito.withSettings().spiedInstance(list).defaultAnswer(Mockito.CALLS_REAL_METHODS).serializable());
	}
	
	@Test
	public void timeoutVerification()
	{
		ArrayList<Integer> list = mock(ArrayList.class);
		when(list.get(1)).thenReturn(10);
		
		list.get(1);
		
		verify(list, timeout(100)).get(1);
	}
}


