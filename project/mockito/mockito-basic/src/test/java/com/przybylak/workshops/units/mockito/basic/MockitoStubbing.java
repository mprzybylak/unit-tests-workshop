package com.przybylak.workshops.units.mockito.basic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.timeout;
import static org.hamcrest.CoreMatchers.not;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.exceptions.verification.SmartNullPointerException;
import org.mockito.internal.stubbing.answers.CallsRealMethods;
import org.mockito.internal.util.MockUtil;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.listeners.InvocationListener;
import org.mockito.listeners.MethodInvocationReport;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

public class MockitoStubbing 
{
	
	private static final int WHEN_RETURN_1 = 10;
	private static final int WHEN_RETURN_2 = 100;
	private static final int WHEN_RETURN_3 = 1000;
	private static final int WHEN_RETURN_4 = 10000;

	private static final int GIVEN_RETURN_1 = 20;
	private static final int GIVEN_RETURN_2 = 200;
	private static final int GIVEN_RETURN_3 = 2000;
	private static final int GIVEN_RETURN_4 = 20000;

	@SuppressWarnings("unchecked")
	@Test
	public void shouldStubReturnValues()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		when(mock.get(1)).thenReturn(WHEN_RETURN_1);
		when(mock.get(2)).thenReturn(WHEN_RETURN_2, WHEN_RETURN_3, WHEN_RETURN_4);
		
		given(mock.get(3)).willReturn(GIVEN_RETURN_1);
		given(mock.get(4)).willReturn(GIVEN_RETURN_2, GIVEN_RETURN_3, GIVEN_RETURN_4);
		
		// when
		int whenResult1 = mock.get(1);
		int whenResult2 = mock.get(2);
		int whenResult3 = mock.get(2);
		int whenResult4 = mock.get(2);
		
		int givenResult1 = mock.get(3);
		int givenResult2 = mock.get(4);
		int givenResult3 = mock.get(4);
		int givenResult4 = mock.get(4);
		
		// then
		assertThat(whenResult1, is(equalTo(WHEN_RETURN_1)));
		assertThat(whenResult2, is(equalTo(WHEN_RETURN_2)));
		assertThat(whenResult3, is(equalTo(WHEN_RETURN_3)));
		assertThat(whenResult4, is(equalTo(WHEN_RETURN_4)));
		
		assertThat(givenResult1, is(equalTo(GIVEN_RETURN_1)));
		assertThat(givenResult2, is(equalTo(GIVEN_RETURN_2)));
		assertThat(givenResult3, is(equalTo(GIVEN_RETURN_3)));
		assertThat(givenResult4, is(equalTo(GIVEN_RETURN_4)));
	}
	
	@Test
	public void shouldCallRealMethodInsteadOfStub()
	{
		// given
		Sum sum = mock(Sum.class);

		when(sum.add(2, 3)).thenCallRealMethod();
		given(sum.add(4, 5)).willCallRealMethod();
		
		// when
		int result1 = sum.add(2, 3);
		int result2 = sum.add(4, 5);
		
		// then
		assertThat(result1, is(equalTo(5)));
		assertThat(result2, is(equalTo(9)));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void shouldThrowException()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		when(mock.get(0)).thenThrow(new IndexOutOfBoundsException());
		given(mock.get(1)).willThrow(new IndexOutOfBoundsException());
		
		/*
		 // this will fail, because get() cannot throw checked exception
		 when(mock.get(3)).thenThrow(new SQLException());
		 given(mock.get(4)).willThrow(new SQLException());
		 */
		
		// when
		try
		{
			mock.get(0);
			fail();
		}
		catch(IndexOutOfBoundsException e) {;}
		
		try
		{
			mock.get(1);
			fail();
		}
		catch(IndexOutOfBoundsException e) {;}
		
	}
	
	@Test
	public void shouldPerformMoreComplicatedAction()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		Answer<Integer> answer = new Answer<Integer>() 
		{
			@Override
			public Integer answer(InvocationOnMock invocation) throws Throwable 
			{
				// additional logic here
				return 1;
			}
		};
		
		when(mock.get(0)).then(answer);
		when(mock.get(1)).thenAnswer(answer);
		
		given(mock.get(2)).will(answer);
		given(mock.get(3)).will(answer);
		
		// when
		int result1 = mock.get(0);
		int result2 = mock.get(1);
		int result3 = mock.get(2);
		int result4 = mock.get(3);
		
		// then
		assertThat(result1, is(equalTo(1)));
		assertThat(result2, is(equalTo(1)));
		assertThat(result3, is(equalTo(1)));
		assertThat(result4, is(equalTo(1)));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void shouldAllowIterativeStubing()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		when(mock.get(0)).thenReturn(1).thenReturn(2).thenReturn(3);
		given(mock.get(1)).willReturn(11).willReturn(12).willReturn(13);
		
		// when
		int whenResult1 = mock.get(0);
		int whenResult2 = mock.get(0);
		int whenResult3 = mock.get(0);
		
		int givenResult1 = mock.get(1);
		int givenResult2 = mock.get(1);
		int givenResult3 = mock.get(1);
		
		// then
		assertThat(whenResult1, is(equalTo(1)));
		assertThat(whenResult2, is(equalTo(2)));
		assertThat(whenResult3, is(equalTo(3)));
		
		assertThat(givenResult1, is(equalTo(11)));
		assertThat(givenResult2, is(equalTo(12)));
		assertThat(givenResult3, is(equalTo(13)));
	}
	
	@Test
	public void shouldStubVoidMethods()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		doCallRealMethod().when(mock).clear();
		doNothing().when(mock).ensureCapacity(10);
		doThrow(new IllegalStateException()).when(mock).trimToSize();
	}
	
}
