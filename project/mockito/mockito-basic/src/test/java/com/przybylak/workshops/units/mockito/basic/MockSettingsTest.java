package com.przybylak.workshops.units.mockito.basic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.withSettings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.MockSettings;
import org.mockito.internal.util.MockName;
import org.mockito.internal.util.MockUtil;
import org.mockito.listeners.InvocationListener;
import org.mockito.listeners.MethodInvocationReport;


/**
 * {@link MockSettings} class allow us to setup few rarely used features of mockito (or features that we usually execures in other way).
 * We can set default answer for mock, we can enforce mock to implement additional interfaces, we can add listeners to invocation
 * of mock's methods, we can add name to mock, we can create serializable mock. We can create spy or we can turn on
 * verbose logging
 */
public class MockSettingsTest 
{
	private static final int SPY_SIZE = 100;
	private static final String SPY_VALUE = "value";
	private static final String NAME = "Name for Mock";
	private static final String LISTENER_INVOCATION = "Listener Invocation";
	
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	/**
	 * Mockito Settings class allow us to set default return answer for nonstubbed methods
	 * of created mock. We need to use {@link MockSettings#defaultAnswer(org.mockito.stubbing.Answer)}.
	 */
	@Test
	public void shouldSetDefaultAnswerToCallRealMethods()
	{
		// given
		Week regularMock = mock(Week.class);
		Week callRealMock = mock(Week.class, withSettings().defaultAnswer(CALLS_REAL_METHODS));

		// when
		DayOfWeek mockResult = regularMock.getToday();
		DayOfWeek callRealResult = callRealMock.getToday();
		
		// then
		assertThat(mockResult, is(nullValue()));
		assertThat(callRealResult, is(not(nullValue())));
	}
	
	/**
	 * In some strange situations we can enforce, that mock will implements additional interface
	 * We need to use {@link MockSettings#extraInterfaces(Class...)}
	 */
	@Test
	public void shouldMockImplementAdditionalInterface()
	{
		// given
		Week mock = mock(Week.class, withSettings().extraInterfaces(NextDayOfWeek.class));
		given(((NextDayOfWeek)mock).getNextDay()).willReturn(DayOfWeek.MONDAY);
		
		// when
		DayOfWeek nextDay = ((NextDayOfWeek)mock).getNextDay();
		
		// then
		assertThat(nextDay, is(equalTo(DayOfWeek.MONDAY)));
	}
	
	/**
	 * We can register listener for every use of mock. We need to use {@link MockSettings#invocationListeners(InvocationListener...)}
	 */
	@Test
	public void shouldAttachInvocationListenerToMock()
	{
		// given
		final StringBuilder sb = new StringBuilder();
		InvocationListener listener = getInvocationListener(sb);
		Week mock = mock(Week.class, withSettings().invocationListeners(listener));
		
		// when
		mock.getToday();
		
		// then
		assertThat(sb.toString(), is(equalTo(LISTENER_INVOCATION)));
	}
	
	/**
	 * We can assign name to mock. To do that we need to use
	 * {@link MockSettings#name(String)}
	 */
	@Test
	public void shouldAddNameToMock()
	{
		// given
		Week mock = mock(Week.class, withSettings().name(NAME));
		
		// when
		MockName mockName = new MockUtil().getMockName(mock);
		
		// then
		assertThat(mockName.toString(), is(equalTo(NAME)));
	}
	
	/**
	 * We can create serializable mocks - we need to use {@link MockSettings#serializable()}
	 */
	@Test
	public void shouldCreateSerializableMock() throws IOException, ClassNotFoundException
	{
		// given
		File f = createFile();
		ObjectOutputStream fos = new ObjectOutputStream(new FileOutputStream(f));
		ObjectInputStream fis = new ObjectInputStream(new FileInputStream(f));
		
		Week mock = mock(Week.class, withSettings().serializable());
		given(mock.getToday()).willReturn(DayOfWeek.TUESDAY);
		
		// when
		fos.writeObject(mock);
		fos.close();
		
		Week deserializedMock = (Week) fis.readObject();
		fis.close();
		
		DayOfWeek mockToday = mock.getToday();
		DayOfWeek deserializedToday = deserializedMock.getToday();
		
		// then
		assertThat(mockToday, is(equalTo(deserializedToday)));
	}
	
	/**
	 * With mock settings we can create spy. We should use {@link MockSettings#spiedInstance(Object)} 
	 * and (don't know why), {@link MockSettings#defaultAnswer(CALLS_REAL_METHODS)}
	 */
	@Test
	public void shouldCreateSpy()
	{
		// given
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> spy = mock(ArrayList.class, withSettings().spiedInstance(list).defaultAnswer(CALLS_REAL_METHODS)); // why it not works without defaultAnswer?
		
		given(spy.size()).willReturn(SPY_SIZE);
		
		// when
		spy.add(SPY_VALUE);
		
		String element = spy.get(0);
		int size = spy.size();
		
		// then
		assertThat(element, is(equalTo(SPY_VALUE)));
		assertThat(size, is(equalTo(SPY_SIZE)));
	}
	
	/**
	 * If we need to "debug" problems with mocks, we can turn on verbose logging with method {@link MockSettings#verboseLogging()}
	 */
	@Test
	public void shouldEnableVerboseLogging()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class, withSettings().verboseLogging());
		given(mock.get(0)).willReturn(100);
		given(mock.get(1)).willReturn(200);
		doNothing().when(mock).clear();
		
		// when
		mock.get(0);
		mock.get(1);
		mock.get(1);
		mock.clear();
		
		// then
		verify(mock, times(1)).get(0);
		verify(mock, atLeast(2)).get(1);
		verify(mock, atMost(1)).clear();
		verifyNoMoreInteractions(mock);
		
		// and now check standard output ;)
	}

	private File createFile() throws IOException 
	{
		return folder.newFile();
	}

	private InvocationListener getInvocationListener(final StringBuilder sb) 
	{
		return new InvocationListener() 
		{
			@Override
			public void reportInvocation(MethodInvocationReport methodInvocationReport) 
			{
				sb.append(LISTENER_INVOCATION);
				System.out.println("Invocation listener work");
			}
		};
	}
}
