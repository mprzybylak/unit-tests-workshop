package com.przybylak.workshops.units.mockito.basic;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import java.util.ArrayList;

import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;


/**
 * Mockito allow us to check if there was interaction with our mocks. We can check that
 * there was/wasn't interaction (in many flavor),  we can also verfiy order of interactions.
 */
public class MockitoVerification 
{
	/**
	 * To verify interactions with mock we need to use {@link Mockito#verify(Object)}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldPerformBasicVerification()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		// when
		mock.add(1);
		mock.clear();
		
		// then
		verify(mock).add(1);
		verify(mock).clear();
	}
	
	/**
	 * Mockito allows for check that some methods were executed at lest N-times with
	 * methods {@link VerificationMode#atLeastOnce()} or 
	 * {@link VerificationMode#atLeast(int minNumberOfInvocations)}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldVerifyAtLeastNUseOfMethod()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		// when
		mock.add(1);
		
		// then
		verify(mock, atLeastOnce()).add(1);
		verify(mock, atLeast(1)).add(1);
	}
	
	/**
	 * Mockito allows for check that some methods were executed at most N-times.
	 * We need to use {@link VerificationMode#atMost(int maxNumberOfInvocations)}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldVerifyAtMostNUseOfMethod()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		// when
		mock.add(1);
		mock.add(1);
		
		// then
		verify(mock, atMost(2)).add(1);
	}
	
	/**
	 * Mockito allows for check that some methods were executed exactly N-times.
	 * We need to use
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldVerifyExactNumberOfUse()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		// when
		mock.add(1);
		mock.add(1);
		
		// then
		verify(mock, times(2)).add(1);
	}
	
	/**
	 * With mockito we can verify that given methods were never used.
	 * Method {@link Mockito#never()} will help
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldVerifyThatMethodWereNeverUsed()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		// when
		mock.add(1);
		
		// then
		verify(mock, never()).clear();
		verify(mock, times(0)).clear();
	}
	
	/**
	 * We can verify that no methods of mock were used at all.
	 * We need to use {@link Mockito#verifyZeroInteractions(Object...)}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldVerfiyNoUseOfMock()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		// then
		verifyZeroInteractions(mock);
	}
	
	/**
	 * After all verification, we can also verify that there was no more
	 * use of given mock. We need to use {@link Mockito#verifyNoMoreInteractions(Object...)}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldVerifyNoMoreUseOfMock()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		// when
		mock.add(1);
		
		// then
		verify(mock).add(1);
		verifyNoMoreInteractions(mock);
	}
	
	/**
	 * If we need, we can check order of methods execution. We need to use object
	 * {@link InOrder} 
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldVerifyInOrder()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		
		// when
		mock.add(1);
		mock.add(2);
		mock.add(3);
		
		// then
		InOrder inOrder = inOrder(mock);
		inOrder.verify(mock).add(1);
		inOrder.verify(mock).add(2);
		inOrder.verify(mock).add(3);
		
		/*
		// this will fail
		inOrder.verify(mock).add(3);
		inOrder.verify(mock).add(2);
		inOrder.verify(mock).add(1);
		 */
	}
}
