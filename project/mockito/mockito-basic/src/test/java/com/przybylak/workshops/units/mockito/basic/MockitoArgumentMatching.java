package com.przybylak.workshops.units.mockito.basic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.intThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.timeout;
import static org.hamcrest.CoreMatchers.not;


import static org.mockito.Matchers.argThat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.exceptions.verification.SmartNullPointerException;
import org.mockito.internal.stubbing.answers.CallsRealMethods;
import org.mockito.internal.util.MockUtil;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.listeners.InvocationListener;
import org.mockito.listeners.MethodInvocationReport;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

public class MockitoArgumentMatching 
{

	@Test
	public void shouldMatchExactArgument()
	{
		// given
		ArrayList<Integer> mock = mock(ArrayList.class);
		given(mock.get(1)).willReturn(100);
		
		// when
		int result = mock.get(1);
		
		// then
		assertThat(result, is(equalTo(100)));
	}
	
	@Test
	public void shouldMatchWithMockitoMatchers()
	{
		// given
		ArrayList<Integer> mockAnyInt = mock(ArrayList.class);
		given(mockAnyInt.get(anyInt())).willReturn(100);
		
		// when
		int anyIntResult1 = mockAnyInt.get(10);
		int anyIntResult2 = mockAnyInt.get(20);
		int anyIntResult3 = mockAnyInt.get(30);
		
		// then
		assertThat(anyIntResult1, is(equalTo(100)));
		assertThat(anyIntResult2, is(equalTo(100)));
		assertThat(anyIntResult3, is(equalTo(100)));
		verify(mockAnyInt.get(anyInt()));
	}
	
	@Test
	public void shouldMatchWithHamcrestMatchers()
	{
		// given
		ArrayList<Integer> mockHamcrest = mock(ArrayList.class);
		HashMap<Object, Object> map = mock(HashMap.class);
		
		given(mockHamcrest.get(intThat(is(equalTo(1))))).willReturn(100);
		given(map.get(argThat(is(not(nullValue()))))).willReturn(new Integer(10));
		
		// when
		int resultHamcrest = mockHamcrest.get(1);
		Integer result = (Integer) map.get(new Integer(0));
		
		// then
		assertThat(resultHamcrest, is(equalTo(100)));
		assertThat(result, is(equalTo(10)));
	}
}
