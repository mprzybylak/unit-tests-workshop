package com.przybylak.workshops.units.mockito.basic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.AtLeast;
import org.mockito.internal.verification.InOrderWrapper;
import org.mockito.internal.verification.MockAwareVerificationMode;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.mockito.verification.VerificationMode;

import static org.mockito.Mockito.*;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.hamcrest.CoreMatchers.is;

@RunWith(MockitoJUnitRunner.class) // we need this runner for mockito annotations
public class MockitoTutorial 
{
	
	
	/**
	 * We can create mock by annotation but we have to use Mockito Runner or
	 * we have to execute {@link MockitoAnnotations.initMocks(testClass)} somewhere in code
	 * manualy
	 */
	@Mock
	private List list;

	/**
	 * Mockito allows to verify if given methods were used
	 * see: {@link Mockito#verify(Object)} 
	 */
	@Test
	public void invocationVerify()
	{
		// creation of mock
		List<Integer> mockedList = mock(List.class);
		List<Integer> orderedMock = mock(List.class);
		List<Integer> notUsed = mock(List.class);
		List<Integer> onlyOneUse = mock(List.class);
		
		// we use mock the same way as regular object
		mockedList.add(5);
		mockedList.clear();	
		
		orderedMock.add(1);
		orderedMock.add(2);
		orderedMock.add(3);
		
		/*
		 * Basic verification
		 */
		
		verify(mockedList).add(5);
		verify(mockedList).clear();
		// verify(mockedList).remove(5); // test will fail - no use of remove
		
		/*
		 * Various modes of verification
		 */
		
		// verification of minimal number of invocation
		verify(mockedList, atLeast(1)).add(5);
		verify(mockedList, atLeastOnce()).add(5);
		
		// verification of maximum number of invocation
		verify(mockedList, atMost(1)).add(5);
		
		// verification of exact number of invocation
		verify(mockedList, times(1)).add(5);

		// order verification
		InOrder order = inOrder(orderedMock);
		order.verify(orderedMock).add(1);
		order.verify(orderedMock).add(2);
		order.verify(orderedMock).add(3);
		/*order.verify(orderedMock).add(3);
		order.verify(orderedMock).add(1);
		order.verify(orderedMock).add(2); */ // wrong order will fail
		
		// verify that method was not used
		verify(mockedList, never()).add(100);

		// verify that mock was not used at all
		verifyZeroInteractions(notUsed);
		
		// we can check if there was more interaction than we expected
		onlyOneUse.add(1);
		// onlyOneUse.add(2); // will cause failure in verifyNoMoreInteractions() method
		verify(onlyOneUse, times(1)).add(1); 
		verifyNoMoreInteractions(onlyOneUse);  
		
	}
	
	/**
	 * We can enforce given method to behave in some way. We use {@link Mockito#when(Object)} 
	 * method for that purpose. In default mock's method returns "default" values, e.g. null
	 * for objects, false for booleans, 0 for decimals, etc.
	 */
	@Test
	public void methodStubing()
	{
		ArrayList<Integer> mockedList = mock(ArrayList.class);

		// no stub - calling real method
		when(mockedList.get(0)).thenCallRealMethod();
		
		// return values
		when(mockedList.get(1)).thenReturn(1);
		when(mockedList.get(2)).thenReturn(1,2,3);
		
		// throw exception on call - we can throw only exception specified in method signature
		when(mockedList.get(3)).thenThrow(new IndexOutOfBoundsException());
		//when(mockedList.get(4)).thenThrow(new SQLException()); // can't throw checked exception
		
		
		// more complex behavior
		when(mockedList.get(5)).thenAnswer(new Answer<Integer>() {
			@Override
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				// some additional logic
				return 5;
			}
		});
		
		// "iterative" stubbing
		when(mockedList.get(6)).thenReturn(1).thenReturn(2).thenThrow(new IndexOutOfBoundsException());
		
		// "iterative" stubbing for one kind of 
	}
	
	@Test
	public void stubbingVoidMethods()
	{
		
	}
	
	@Test
	public void argumentMatching()
	{
		List<Integer> mockedList = mock(List.class);
		
		// "regular" argument matching - it will use equals() method
		when(mockedList.get(1)).thenReturn(1);
		
		// mockito matchers - all methods starts with any*();
		when(mockedList.get(anyInt())).thenReturn(2);
		
		// we can use hamcrest matchers with *That() method
		// for example for primitive int: intThat, for primitive float:
		// floatThat(), for object anyThat() - if we use anyThat with
		// primitivies it will bring NPE on runtime
		when(mockedList.get(intThat(is(greaterThan(12))))).thenReturn(100);
		
	}
}
