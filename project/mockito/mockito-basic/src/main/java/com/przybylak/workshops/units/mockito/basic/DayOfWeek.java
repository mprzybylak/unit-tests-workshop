package com.przybylak.workshops.units.mockito.basic;

public enum DayOfWeek 
{
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY;
}
