package com.przybylak.workshops.units.mockito.basic;

public interface NextDayOfWeek 
{
	public DayOfWeek getNextDay();
}
