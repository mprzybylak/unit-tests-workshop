package com.przybylak.workshops.units.mockito.basic;

import java.util.Calendar;

public class Week 
{
	public DayOfWeek getToday()
	{
		Calendar c = Calendar.getInstance();
		return DayOfWeek.values()[c.get(Calendar.DAY_OF_WEEK)-1];
	}
}
