package com.przybylak.workshops.units.dbunit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.junit.Test;

import com.przybylak.workshops.units.dbunit.base.DbUnitTestCase;

public class DbUnitsTutorial extends DbUnitTestCase
{
	private static final String SQL = "CREATE TABLE hrapptest (id BIGINT NOT NULL, empcode VARCHAR(15), loginname VARCHAR(30), loginenabled VARCHAR(1))";
	private static final String DROP = "DROP TABLE hrapptest";
	private static final String FILE_NAME = "input.xml";

	@Test
	public void testTest() throws SQLException, DatabaseUnitException
	{
		IDataSet dataSet = new DatabaseConnection(connection).createDataSet();
		
		assertNotNull(dataSet);
		int rowCount = dataSet.getTable("hrapptest").getRowCount();
		assertEquals(2, rowCount);
	}

	@Override
	public String getDerbyDatabaseName() 
	{
		return "dbunits-test";
	}

	@Override
	public String[] getTablesCreationStrings() 
	{
		return new String[]{SQL};
	}

	@Override
	public String getXmlName() {
		return FILE_NAME;
	}

	@Override
	public String[] getTableDropStrings() 
	{
		return new String[]{DROP};
	}
}
