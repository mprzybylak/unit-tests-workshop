package com.przybylak.workshops.units.dbunit.base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;

public abstract class DbUnitTestCase 
{
	protected static final String CREATION_SUFFIX = ";create=true";
	protected static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	protected static final String JDBC_ADDRESS = "jdbc:derby:memory:";
	protected static final String SHUTDOWN_SUFFIX = ";shutdown=true";

	private FlatXmlDataSet dataSet;
	protected Connection connection;

	@Before
	public void setUp()
	{
		Connection inMemoryDbConnection = createInMemoryDatabase();
		createSchema(inMemoryDbConnection);
		configureDbUnit(inMemoryDbConnection);
	}
	
	@After
	public void tearDown()
	{
		deleteSchema(connection);
		destroyDb();
	}

	private void deleteSchema(Connection connection) 
	{
		try
		{
			Statement statement = connection.createStatement();
			for(String sql : getTableDropStrings())
			{
				statement.execute(sql);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}


	private void destroyDb() {
		try 
		{
			connection.close();
			DriverManager.getConnection(JDBC_ADDRESS + getDerbyDatabaseName() + SHUTDOWN_SUFFIX);
		} 
		catch (SQLException e) 
		{
			assertEquals("08006", e.getSQLState());
		}
	}
	
	private void configureDbUnit(Connection inMemoryDbConnection) 
	{
		try 
		{
			dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream(getXmlName()));
			DatabaseOperation.CLEAN_INSERT.execute(new DatabaseConnection(inMemoryDbConnection), dataSet);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	private void createSchema(Connection connection) 
	{
		try 
		{
			Statement statement = connection.createStatement();
			for(String sql : getTablesCreationStrings())
			{
				statement.execute(sql);
			}
			
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
	}

	private Connection createInMemoryDatabase()
	{
		
		try 
		{
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(JDBC_ADDRESS + getDerbyDatabaseName() + CREATION_SUFFIX);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		
		return connection;
	}

	public abstract String getDerbyDatabaseName();
	public abstract String[] getTablesCreationStrings();
	public abstract String getXmlName();
	public abstract String[] getTableDropStrings();
}
