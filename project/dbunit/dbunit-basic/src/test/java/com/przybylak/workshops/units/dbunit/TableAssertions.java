package com.przybylak.workshops.units.dbunit;

import java.io.IOException;
import java.math.BigInteger;
import java.sql.SQLException;
import java.sql.Statement;

import org.dbunit.Assertion;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.CompositeTable;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.FilteredTableMetaData;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.IRowValueProvider;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.RowFilterTable;
import org.dbunit.dataset.SortedTable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.filter.IRowFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;

import com.przybylak.workshops.units.dbunit.base.DbUnitTestCase;

public class TableAssertions extends DbUnitTestCase 
{

	private static final String SQL = "CREATE TABLE hrapptest (id BIGINT NOT NULL, empcode VARCHAR(15), loginname VARCHAR(30), loginenabled VARCHAR(1))";
	private static final String DROP = "DROP TABLE hrapptest";
	
	@Test
	public void tableAssertion() throws IOException, DatabaseUnitException, SQLException
	{
		Statement statement = connection.createStatement();
		statement.execute("INSERT INTO hrapptest VALUES (3,'E007', 'johndoe','y')");

		IDataSet acctualDataSet = new DatabaseConnection(connection).createDataSet();
		ITable acctualTable = acctualDataSet.getTable("hrapptest");

		FlatXmlDataSet expectedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResource("input-assertions2.xml"));
		ITable expectedTable = expectedDataSet.getTable("hrapptest");
		
		// table assertions
		Assertion.assertEquals(expectedTable, acctualTable);
		
		// whole dataset assertion
		Assertion.assertEquals(expectedDataSet, acctualDataSet);
	}
	
	@Test
	public void tableAssertionsExcludeColumns() throws SQLException, IOException, DatabaseUnitException
	{
		Statement statement = connection.createStatement();
		statement.execute("INSERT INTO hrapptest VALUES (3,'E007', 'johndoe','y')");
		
		IDataSet acctualDataSet = new DatabaseConnection(connection).createDataSet();
		ITable acctualTable = acctualDataSet.getTable("hrapptest");
		
		// filter
		DefaultColumnFilter filter = new DefaultColumnFilter();
		filter.excludeColumn("id");
		
		// filtered metadata
		FilteredTableMetaData metaData = new FilteredTableMetaData(acctualTable.getTableMetaData(), filter);
		
		FlatXmlDataSet expectedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResource("input-assertions2.xml"));
		ITable expectedTable = expectedDataSet.getTable("hrapptest");
		
		// assertion with new metadata
		Assertion.assertEquals(new CompositeTable(metaData, expectedTable), new CompositeTable(metaData, acctualTable));
	}
	
	@Test
	public void tableAssertionsRowFilter() throws SQLException, DatabaseUnitException, IOException
	{
		Statement statement = connection.createStatement();
		statement.execute("INSERT INTO hrapptest VALUES (3,'E007', 'johndoe','y')");
		
		IDataSet acctualDataSet = new DatabaseConnection(connection).createDataSet();
		ITable acctualTable = acctualDataSet.getTable("hrapptest");
		
		// filter
		IRowFilter filter = new IRowFilter() 
		{
			@Override
			public boolean accept(IRowValueProvider arg0) 
			{
				try 
				{
					Object columnValue2 = arg0.getColumnValue("id");
					
					if(columnValue2 instanceof BigInteger && ((BigInteger)columnValue2).equals(new BigInteger("1")))
						return false;
					
					if(columnValue2 instanceof String && ((String)columnValue2).equals("1"))
						return false;
				} 
				catch (DataSetException e) 
				{
					e.printStackTrace();
				}
				return true;
			}
		};
		
		FlatXmlDataSet expectedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResource("input-assertions2.xml"));
		ITable expectedTable = expectedDataSet.getTable("hrapptest");
		
		Assertion.assertEquals(new RowFilterTable(expectedTable, filter), new RowFilterTable(acctualTable, filter));
	}
	
	/**
	 * If db unit can order tables by itself - we need to order table manualy. If we don't sort tables - assertion will fail
	 * @throws SQLException 
	 * @throws DatabaseUnitException 
	 * @throws IOException 
	 */
	@Test
	public void rowOrdering() throws SQLException, DatabaseUnitException, IOException
	{
		Statement statement = connection.createStatement();
		statement.execute("INSERT INTO hrapptest VALUES (3,'E007', 'johndoe','y')");
		
		IDataSet acctualDataSet = new DatabaseConnection(connection).createDataSet();
		ITable acctualTable = acctualDataSet.getTable("hrapptest");
		
		FlatXmlDataSet expectedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResource("input-assertions2.xml"));
		ITable expectedTable = expectedDataSet.getTable("hrapptest");
		
		Assertion.assertEquals(new SortedTable(expectedTable), new SortedTable(acctualTable));
	}
	
	@Override
	public String getDerbyDatabaseName() 
	{
		return "dbunits-assertions";
	}

	@Override
	public String[] getTablesCreationStrings() 
	{
		return new String[] {SQL};
	}

	@Override
	public String getXmlName() 
	{
		return "input-assertions1.xml";
	}

	@Override
	public String[] getTableDropStrings() 
	{
		return new String[]{DROP};
	}

}
