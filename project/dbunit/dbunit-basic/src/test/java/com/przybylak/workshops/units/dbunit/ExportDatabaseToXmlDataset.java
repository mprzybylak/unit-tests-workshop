package com.przybylak.workshops.units.dbunit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.database.search.TablesDependencyHelper;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatDtdDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.util.search.SearchException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.przybylak.workshops.units.dbunit.base.DbUnitTestCase;

public class ExportDatabaseToXmlDataset extends DbUnitTestCase
{

	private static final String SQL = "CREATE TABLE hrapptest (id BIGINT NOT NULL, empcode VARCHAR(15), loginname VARCHAR(30), loginenabled VARCHAR(1))";
	private static final String SQL2 = "CREATE TABLE hrapptest2 (id BIGINT NOT NULL, empcode VARCHAR(15), loginname VARCHAR(30), loginenabled VARCHAR(1))";
	
	private static final String DROP = "DROP TABLE hrapptest";
	private static final String DROP2 = "DROP TABLE hrapptest2";
	
	private static final String FILE_NAME = "input-export.xml";
	
	@Rule
	public TemporaryFolder folder = new TemporaryFolder(); 

	@Override
	public String getDerbyDatabaseName() 
	{
		return "database-export";
	}

	@Override
	public String[] getTablesCreationStrings() 
	{
		return new String[]{SQL, SQL2};
	}

	@Override
	public String getXmlName() {
		return FILE_NAME;
	}
	
	@Test
	public void partialExport() throws SQLException, FileNotFoundException, IOException, DatabaseUnitException
	{
		IDatabaseConnection iConnection = new DatabaseConnection(connection);
		QueryDataSet partialDataSet = new QueryDataSet(iConnection);
		
		// full table
		partialDataSet.addTable("hrapptest");
		
		// part of table
		partialDataSet.addTable("hrapptest2", "SELECT * FROM hrapptest2 WHERE loginenabled = 'y'");
		
		// write to file
		File newFile = folder.newFile("exported-partial.xml");
		FlatXmlDataSet.write(partialDataSet, new FileOutputStream(newFile));
	}
	
	@Override
	public String[] getTableDropStrings() 
	{
		return new String[]{DROP, DROP2};
	}
	
	@Test
	public void partialExportWithDependencies() throws SQLException, IOException, DatabaseUnitException
	{
		IDatabaseConnection iConnection = new DatabaseConnection(connection);
		
		// we will get table 'hrapptest' and it's all dependencies (in this case there is no dependencies)
		String[] dependencyTables = TablesDependencyHelper.getAllDependentTables(iConnection, "hrapptest");
		IDataSet dependencyDataset = iConnection.createDataSet(dependencyTables);
		
		// write to file
		File newFile = folder.newFile("exported-partial-dependencies.xml");
		FlatXmlDataSet.write(dependencyDataset, new FileOutputStream(newFile));
	}
	
	@Test
	public void fullExport() throws SQLException, IOException, DatabaseUnitException
	{
		IDatabaseConnection iConnection = new DatabaseConnection(connection);
		
		// full database
		IDataSet fullDataset = iConnection.createDataSet();
		
		// write to file
		File newFile = folder.newFile("exported-full.xml");
		FlatXmlDataSet.write(fullDataset, new FileOutputStream(newFile));
	}
	
	@Test
	public void exportDTD() throws FileNotFoundException, IOException, DatabaseUnitException, SQLException
	{
		IDatabaseConnection iConnection = new DatabaseConnection(connection);
		
		// full database
		IDataSet fullDataset = iConnection.createDataSet();
		
		// write to file
		File newFile = folder.newFile("exported-full.xml");
		FlatDtdDataSet.write(fullDataset, new FileOutputStream(newFile));
	}

}
