package com.przybylak.workshops.units.junits.custom;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Most common method to provide our own matcher is to extend TypeSafeMatcher class.
 * TypeSafeMatcher provides null-checking of arguments, so we don't need to do that.
 * Also TypeSafeMatcher will cast object to type defined by us. We have to override 
 * two methods - describeTo and matchesSafely.
 */
public class IsNotANumber extends TypeSafeMatcher<Double> 
{

	/* (non-Javadoc)
	 * @see org.hamcrest.SelfDescribing#describeTo(org.hamcrest.Description)
	 * describeTo method alows us to generate description of object
	 */
	@Override
	public void describeTo(Description description) 
	{
		description.appendText("is not a number");
	}

	/* (non-Javadoc)
	 * @see org.hamcrest.TypeSafeMatcher#matchesSafely(java.lang.Object)
	 * matchesSafely method is used to do actual matching
	 */
	@Override
	protected boolean matchesSafely(Double number) {
		return number.isNaN();
	}
	
	/**
	 * Method that allows us to easy creation of this kind of matcher
	 * annotation @Factory is optional. If we use it - we allows hamcrest
	 * to automatic generation header which combine all matcher, so we
	 * can use just one import
	 */
	@Factory
	public static <T> Matcher<Double> notNumber()
	{
		return new IsNotANumber();
	}

}
