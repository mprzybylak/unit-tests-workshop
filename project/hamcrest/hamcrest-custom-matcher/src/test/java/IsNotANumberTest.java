import static com.przybylak.workshops.units.junits.custom.IsNotANumber.notNumber;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;


/**
 * An example of usage of custom hamcrest matcher IsNotANumber
 */
public class IsNotANumberTest 
{
	@Test
	public void shouldCheckIfNumberIsNotANumber()
	{
		// given
		Double number = 44.0;
		Double notANumber = Double.NaN;
		
		// when
		
		// then
		assertThat(number, is(not(notNumber())));
		assertThat(notANumber, is(notNumber()));
	}
}
