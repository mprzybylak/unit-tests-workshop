package com.przybylak.workshops.units.hamcrest.math;

import java.util.ArrayList;
import java.util.Collection;

public class EvenNumberGenerator 
{
	private EvenNumberGenerator()
	{
		;
	}
	
	public static Integer[] generateNumber(int n)
	{
		Integer[] evenArray = new Integer[n];
		
		for(int i = 0; i < n; ++i)
		{
			evenArray[i] = (i+1) * 2;
		}
		
		return evenArray;
	}
	
	public static Collection<Integer> generateNumberCollection(int n)
	{
		Collection<Integer> collection = new ArrayList<Integer>(n);
		for(int i = 0; i < n; ++i)
		{
			collection.add((i+1)*2);
		}
		return collection;
	}
}
