package com.przybylak.workshops.units.hamcrest.person;

/**
 * Comparable Person is a Smart Person that can be ordered by IQ
 */
public class ComparablePerson extends SmartPerson implements Comparable<ComparablePerson> 
{

	public ComparablePerson(String firstName, String lastName, int age, int iq) 
	{
		super(firstName, lastName, age, iq);
	}

	@Override
	public int compareTo(ComparablePerson otherPerson) 
	{
		if(iq > otherPerson.iq)
		{
			return 1;
		}
		else if(iq < otherPerson.iq)
		{
			return -1;
		}
		return 0;
	}

}
