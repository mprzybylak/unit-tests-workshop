package com.przybylak.workshops.units.hamcrest.math;

public class Divider 
{
	public static double divide(final double x, final double y)
	{
		return x/y;
	}
}
