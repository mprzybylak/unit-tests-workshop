package com.przybylak.workshops.units.hamcrest.person;

public class Person 
{
	private String firstName;
	private String lastName;
	private int age;
	
	public Person(String firstName, String lastName, int age)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(this == obj)
		{
			return true;
		}
		
		if(!(obj instanceof Person))
		{
			return false;
		}
		
		Person otherPerson = (Person)obj;
		if(firstName != otherPerson.firstName)
		{
			return false;
		}
		
		if(lastName != otherPerson.lastName)
		{
			return false;
		}
		
		if(age != otherPerson.age)
		{
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() 
	{
		return firstName + " " + lastName;
	}
	
	public String getName()
	{
		return firstName;
	}
}
