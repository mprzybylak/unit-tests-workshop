package com.przybylak.workshops.units.hamcrest.person;

public class SmartPerson extends Person 
{
	protected int iq;
	
	public SmartPerson(String firstName, String lastName, int age) 
	{
		this(firstName, lastName, age, 0);
	}

	public SmartPerson(String firstName, String lastName, int age, int iq) 
	{
		super(firstName, lastName, age);
		this.iq = iq;
	}
}
