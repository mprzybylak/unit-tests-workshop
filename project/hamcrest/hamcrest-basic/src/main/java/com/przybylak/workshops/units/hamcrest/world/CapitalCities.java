package com.przybylak.workshops.units.hamcrest.world;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CapitalCities 
{

	private static Map<String, String> capitals;
	
	static
	{
		capitals = new HashMap<String, String>();
		capitals.put("Poland", "Warsaw");
		capitals.put("Germany", "Berlin");
		capitals.put("England", "London");
	}
	
	public static Map<String, String> getAllCapitals()
	{
		return Collections.unmodifiableMap(capitals);
	}
}
