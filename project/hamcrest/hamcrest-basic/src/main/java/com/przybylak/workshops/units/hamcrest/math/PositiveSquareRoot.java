package com.przybylak.workshops.units.hamcrest.math;

public class PositiveSquareRoot 
{
	public static Double computeSquareRoot(Double n)
	{
		if(n > 0)
		{
			return Math.sqrt(n);
		}
		return null;
	}
}
