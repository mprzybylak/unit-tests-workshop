package com.przybylak.workshops.units.hamcrest.bean;

import java.util.EventObject;

public class DummyBean 
{
	private int firstProperty;
	private double secondProperty;
	
	public double getSecondProperty() 
	{
		return secondProperty;
	}
	
	public void setSecondProperty(double secondProperty) 
	{
		this.secondProperty = secondProperty;
	}
	
	public int getFirstProperty() 
	{
		return firstProperty;
	}
	
	public void setFirstProperty(int firstProperty) 
	{
		this.firstProperty = firstProperty;
	}
	
	public class DummyEvent extends EventObject
	{
		private static final long serialVersionUID = -1279840219027161056L;

		public DummyEvent(Object source) {
			super(source);
		}
	}
	
}
