package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.describedAs;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.math.PositiveSquareRoot;
import com.przybylak.workshops.units.hamcrest.person.Person;
import com.przybylak.workshops.units.hamcrest.person.SmartPerson;

/**
 * Hamcrest allows to check general object characteristic, such as class,
 * "nullity" of reference and so on.
 */
public class ObjectMatching 
{
	/**
	 * For checking instances we can use methods: {@link Matchers#is(Class)},
	 * {@link Matchers#isA(Class)} {@link Matchers#instanceOf(Class)} or
	 * {@link Matchers#any(Class)}
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void shouldCheckClassOfObjects()
	{
		Person regularPerson = new Person("John", "Doe", 26);
		Person smartPerson = new SmartPerson("Judy", "Smart", 18);
		
		assertThat(smartPerson, is(Person.class)); // deprecated
		assertThat(smartPerson, isA(Person.class));
		assertThat(smartPerson, is(instanceOf(Person.class)));
		assertThat(smartPerson, is(IsInstanceOf.<Person>any(Person.class)));
		
		assertThat(regularPerson, not(instanceOf(SmartPerson.class)));
		assertThat(regularPerson, is(not(instanceOf(SmartPerson.class))));
	}
	
	/**
	 * Methods like {@link Matchers#nullValue()},
	 * {@link Matchers#notNullValue()} allows us to check value of reference
	 */
	@Test
	public void shouldComputeProperPositiveSquareRoot()
	{
		Double sixteen = new Double(16);
		Double minusOne = new Double(-1);
		
		Double four = PositiveSquareRoot.computeSquareRoot(sixteen);
		Double imaginary = PositiveSquareRoot.computeSquareRoot(minusOne);
		
		assertThat(four, is(notNullValue()));
		assertThat(four, is(notNullValue(Double.class)));
		assertThat(imaginary, is(nullValue()));
		assertThat(imaginary, is(nullValue(Double.class)));
	}
	
	/**
	 * For instances comparision we can use {@link Matchers#sameInstance}
	 */
	@Test
	public void shouldCompareInstances()
	{
		Integer twentyFirst = 12;
		Integer twentySec = 12;

		assertThat(twentyFirst, is(sameInstance(twentySec)));
		assertThat(twentyFirst, describedAs("We can set custom description to assertion", is(equalTo(12))));
	}
}
