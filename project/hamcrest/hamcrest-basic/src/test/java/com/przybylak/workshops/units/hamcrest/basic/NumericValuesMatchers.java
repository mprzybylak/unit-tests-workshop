package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.hamcrest.number.IsCloseTo;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.math.Divider;

/**
 * Hamcrest give us few methods to comparsion integral and floating point numbers
 */
public class NumericValuesMatchers 
{
	/**
	 * {@link Matchers#equalTo}, 
	 * {@link Matchers#greaterThan}, {@link Matchers#greaterThanOrEqualTo}, 
	 * {@link Matchers#lessThan}, {@link Matchers#lessThanOrEqualTo}
	 * are equivalent of mathematic operators =, >, >=, <, <=
	 */
	@Test
	public void shouldEvaluateNumers()
	{
		int two = 2;
		int six = 6;
		int four = 4;
		
		assertThat(two, is(equalTo(2)));
		assertThat(six, is(greaterThan(four)));
		assertThat(six, is(greaterThanOrEqualTo(four)));
		assertThat(four, is(lessThan(six)));
		assertThat(four, is(lessThanOrEqualTo(six)));
	}
	
	/**
	 * We can check equality of two floating point values with {@link IsCloseTo#closeTo}
	 */
	@Test
	public void shouldEvaluateFloatingPointsValues()
	{
		double result = Divider.divide(2, 3);
		
		assertThat(result, is(closeTo(0.6, 0.1)));

		// bias works in both ways
		assertThat(result, is(closeTo(0.5, 0.2))); // lower than result
		assertThat(result, is(closeTo(0.7, 0.2))); // higher in result
	}
}
