package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.eventFrom;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.hamcrest.beans.HasProperty;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.beans.SamePropertyValuesAs;
import org.hamcrest.object.IsEventFrom;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.bean.DummyBean;

/**
 * <p>
 * Hamcrest gives us mechanisms to evaluate Java Beans:
 * </p>
 * <ul>
 * <li>check if bean has property</li>
 * <li>check if bean's property has given value</li>
 * <li>check if two beans has the same values of properties</li>
 * <li>check bean's events</li>
 * </ul>
 */
public class BeanMatching 
{
	/**
	 * If we want to check if bean has property, we can use
	 * {@link HasProperty#hasProperty}
	 */
	@Test
	public void shouldCheckIfBeanHasProperty()
	{
		DummyBean bean = new DummyBean();
		
		assertThat(bean, hasProperty("firstProperty"));
	}
	
	/**
	 * We can check if property has given value with
	 * {@link HasPropertyWithValue#hasProperty}
	 */
	@Test
	public void shouldCheckIfBeanHasPropertyWithValue()
	{
		DummyBean bean = new DummyBean();
		bean.setFirstProperty(10);
		
		assertThat(bean, HasPropertyWithValue.hasProperty("firstProperty", equalTo(10)));
	}
	
	/**
	 * We can compare properties of two beans with
	 * {@link SamePropertyValuesAs#samePropertyValuesAs} method
	 */
	@Test
	public void shouldCheckEqualityOfBeansProperties()
	{
		DummyBean firstBean = new DummyBean();
		DummyBean secBean = new DummyBean();
		DummyBean thirdBean = new DummyBean();

		firstBean.setFirstProperty(10);
		secBean.setFirstProperty(10);
		thirdBean.setFirstProperty(30);
		
		assertThat(firstBean, SamePropertyValuesAs.samePropertyValuesAs(secBean));
		assertThat(firstBean, not(SamePropertyValuesAs.samePropertyValuesAs(thirdBean)));
	}
	
	/**
	 * We can check if event come from given bean with
	 * {@link IsEventFrom#eventFrom}
	 */
	@Test
	public void shouldEvaluateEvent()
	{
		DummyBean source = new DummyBean();
		DummyBean.DummyEvent ev = new DummyBean().new DummyEvent(source);
		
		assertThat(ev, is(eventFrom(source)));
		
		// additional check of event class
		assertThat(ev, is(eventFrom(DummyBean.DummyEvent.class, source)));
	}
}
