package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.hasXPath;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.hamcrest.xml.HasXPath;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * With hamcrest we can evaluate xpath of XMLs
 */
public class XmlMatching 
{
	/**
	 * We can use {@link HasXPath#hasXPath} for evaluate given xml
	 */
	@Test
	public void shouldEvaluateXpath() throws ParserConfigurationException, SAXException, IOException
	{
		String testXm = "<xml><foo><dummy><element/></dummy></foo></xml>";
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		ByteArrayInputStream bis = new ByteArrayInputStream(testXm.getBytes());
		
		Document doc = db.parse(bis);
		
		assertThat(doc, hasXPath("xml/foo/dummy/element"));
	}
}
