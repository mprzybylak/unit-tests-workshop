package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.array;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.arrayContainingInAnyOrder;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.hamcrest.collection.IsArrayContaining;
import org.hamcrest.collection.IsArrayContainingInAnyOrder;
import org.hamcrest.collection.IsArrayContainingInOrder;
import org.hamcrest.collection.IsArrayWithSize;
import org.hamcrest.collection.IsIn;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.math.EvenNumberGenerator;

/**
 * <p>
 * Hamcrest allows us to evaluate arrays:
 * </p>
 * <ul>
 * <li>is array empty</li>
 * <li>is array contains given element(s)</li>
 * <li>is given element is in array</li>
 * <li>array size</li>
 * <li>is each element of array fullfill given requirements</li>
 * </ul>
 */
public class ArraysMatching 
{
	/**
	 * We can evaluate if array is empty with method
	 * {@link IsArrayWithSize#emptyArray()}
	 */
	@Test
	public void shouldEvaluateIfArrayIsEmpty()
	{
		Integer[] generateNumber = EvenNumberGenerator.generateNumber(5);
		Integer[] emptyArray = EvenNumberGenerator.generateNumber(0);
		
		assertThat(emptyArray, is(emptyArray()));
		
		// there isn't matcher for not empty arrays, but we can use not()
		assertThat(generateNumber, is(not(emptyArray()))); 
	}
	
	/**
	 * We can check if given element exist in array or if any element in array
	 * fulfill given requirement with {@link IsArrayContaining#hasItemInArray}
	 */
	@Test
	public void shouldCheckIfArrayHasItem()
	{
		Integer[] generateNumber = EvenNumberGenerator.generateNumber(5);

		// checking if given element is in array
		assertThat(generateNumber, hasItemInArray(2));
		assertThat(generateNumber, hasItemInArray(4));
		assertThat(generateNumber, hasItemInArray(6));

		// checking if any element from array is greater than 2
		assertThat(generateNumber, hasItemInArray(greaterThan(2)));
	}
	
	/**
	 * We can check if array contains given elements with: 
	 * {@link IsArrayContainingInOrder#arrayContaining} or
	 * {@link IsArrayContainingInAnyOrder#arrayContainingInAnyOrder}
	 */
	@Test
	public void shouldEvaluateContentOfArray()
	{
		Integer[] generateNumber = EvenNumberGenerator.generateNumber(5);
		
		assertThat(generateNumber, is(arrayContaining(2,4,6,8,10)));
		
		// if we don't care about order
		assertThat(generateNumber, is(arrayContainingInAnyOrder(2,6,10,8,4)));
	}

	/**
	 * We can check if given value is an element of array also with
	 * {@link IsIn#isIn} method
	 */
	@Test
	public void shouldCheckIfElementIsInCollection()
	{
		Integer[] generateNumber = EvenNumberGenerator.generateNumber(5);

		assertThat(2, isIn(generateNumber));
	}
	
	/**
	 * We can check size of array with {@link IsArrayWithSize#arrayWithSize}
	 */
	@Test
	public void shouldEvaluateSizeOfArray()
	{
		Integer[] generateNumber = EvenNumberGenerator.generateNumber(5);
		
		assertThat(generateNumber, is(arrayWithSize(5)));
		
		// we can use additional matcher
		assertThat(generateNumber, is(arrayWithSize(lessThan(10))));
	}
	
	/**
	 * Sometimes we need to check each of element in array with different
	 * condition. {@link IsArray#array} method allows us to do that
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldEvaluateMatcherForEachElementOfArray()
	{
		Integer[] generateNumber = EvenNumberGenerator.generateNumber(5);
		
		// we have to pass matcher for each array element
		assertThat(generateNumber, is(array(is(2), is(4), is(6), is(8), is(10))));
	}
	
}
