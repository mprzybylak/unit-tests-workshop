package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.world.CapitalCities;

/**
 * <p>
 * With hamcrest we can examinate maps:
 * </p>
 * <ul>
 * <li>entries</li>
 * <li>keys</li>
 * <li>values</li>
 * </ul>
 */
public class MapMatching 
{
	/**
	 * {@link IsMapContaining#hasEntry} checks if map has given entry
	 */
	@Test
	public void shouldEvaluateEntriesInMap()
	{
		Map<String, String> allCapitals = CapitalCities.getAllCapitals();
		
		assertThat(allCapitals, hasEntry("Poland", "Warsaw"));
	}
	
	/**
	 * {@link IsMapContaining#hasKey} checks if map has given key
	 */
	@Test
	public void shouldEvaluateKeyInMap()
	{
		Map<String, String> allCapitals = CapitalCities.getAllCapitals();
		
		assertThat(allCapitals, hasKey("Germany"));
		assertThat(allCapitals, hasKey(startsWith("Ger")));
	}
	
	/**
	 * {@link IsMapContaining#hasValue} checks if map has given value
	 */
	@Test
	public void shouldEvaluateValueInMap()
	{
		Map<String, String> allCapitals = CapitalCities.getAllCapitals();
		
		assertThat(allCapitals, hasValue("London"));
	}
}
