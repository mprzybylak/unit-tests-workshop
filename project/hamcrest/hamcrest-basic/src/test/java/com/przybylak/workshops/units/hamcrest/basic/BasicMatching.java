package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.hamcrest.core.IsAnything;
import org.junit.Assert;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.person.ComparablePerson;
import com.przybylak.workshops.units.hamcrest.person.Person;

/**
 * Most common way to use hamcrest is to use its static methods with Junit's
 * {@link Assert#assertThat(Object, org.hamcrest.Matcher)}
 */
public class BasicMatching 
{
	/**
	 * One of the most basic assertion is checking of (not)equality of two objects
	 * hamcrest provides methods {@link Matchers#equalTo(Object)} for this
	 */
	@Test
	public void shouldCheckIfTwoPersonsAreEqual()
	{
		// given
		Person firstPerson = new Person("John", "Doe", 37);
		Person secondPerson = new Person("John", "Doe", 37);
		Person thirdPerson = new Person("Jane", "Doe", 25);
		
		// then
		
		/*
		 * There are several way to express this equality assertion
		 * method "is" is syntax sugar which allows us to provide
		 * more readable expressions
		 */
		assertThat(firstPerson, is(secondPerson));
		assertThat(firstPerson, equalTo(secondPerson));
		assertThat(firstPerson, is(equalTo(secondPerson)));
		
		/*
		 * not() method allows as to reverse matching statement
		 */
		assertThat(firstPerson, not(thirdPerson));
		assertThat(firstPerson, is(not(thirdPerson)));
		assertThat(firstPerson, not(equalTo(thirdPerson)));
		assertThat(firstPerson, is(not(equalTo(thirdPerson))));
	}
	
	/**
	 * We can test objects that implement {@link Comparable} interface
	 */
	@Test
	public void shouldCheckIfTwoPersonsAreEqualByComparsion()
	{
		ComparablePerson person = new ComparablePerson("John", "Doe", 20, 130);
		ComparablePerson equalPerson = new ComparablePerson("Jane", "Doe", 23, 130);
		ComparablePerson nonEqualPerson = new ComparablePerson("Josh", "Doe", 20, 120);
		
		assertThat(person, is(comparesEqualTo(equalPerson)));
		assertThat(person, is(not(comparesEqualTo(nonEqualPerson))));
	}
	
	/**
	 * {@link IsAnything#anything} matcher will allways evalute to true
	 */
	@Test
	public void shoulThisTestAlwaysPass()
	{
		Person person = new Person("John", "Doe", 21);
		
		assertThat(person, is(anything()));
	}
	
}
