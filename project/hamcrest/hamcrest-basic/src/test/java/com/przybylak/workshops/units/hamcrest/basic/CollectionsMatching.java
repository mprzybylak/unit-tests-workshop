package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.Collection;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.collection.IsEmptyIterable;
import org.hamcrest.collection.IsIn;
import org.hamcrest.collection.IsIterableWithSize;
import org.hamcrest.core.Every;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.math.EvenNumberGenerator;

/**
 * <p>
 * Hamcrest allows us to evaluate collections. 
 * </p>
 * <ul>
 * <li>check if every items fulfill requirement</li>
 * <li>check if given element is in collection</li>
 * <li>check if collection is empty</li>
 * <li>check if collection contain given element(s)</li>
 * <li>check if collection has given size</li>
 * </ul>
 * <p>
 * Many method comes in two flavors:
 * </p>
 * <ul>
 * <li>for Collection interface</li>
 * <li>for Iterable interface</li>
 * </ul>
 */
public class CollectionsMatching 
{
	/**
	 * We can check if every item in collection fulfill given requirement with
	 * {@link Every#everyItem(org.hamcrest.Matcher)}
	 */
	@Test 
	public void shouldEvaluateEveryItemInCollection()
	{
		Collection<Integer> generateCollection = EvenNumberGenerator.generateNumberCollection(3);
		
		assertThat(generateCollection, everyItem(is(greaterThan(0))));
	}
	
	/**
	 * Like for arrays - {@link IsIn#isIn} allows us to check if given element
	 * is in collection
	 */
	@Test
	public void shouldCheckIfElementIsInCollection()
	{
		Collection<Integer> generateCollection = EvenNumberGenerator.generateNumberCollection(3);
		
		assertThat(2, isIn(generateCollection));
	}
	
	/**
	 * We can check if collection/iterable is empty with methods
	 * {@link IsEmptyCollection#empty()} and {@link IsEmptyIterable#emptyIterable()}
	 */
	@Test
	public void shouldCheckIfCollectionIsEmpty()
	{
		Collection<Integer> generateCollection = EvenNumberGenerator.generateNumberCollection(3);
		Collection<Integer> emptyCollection = EvenNumberGenerator.generateNumberCollection(0);

		// collections
		assertThat(emptyCollection, is(Matchers.<Integer>empty())); 
		assertThat(generateCollection, is(not(Matchers.<Integer>empty())));
		
		// iterable
		assertThat(emptyCollection, is(Matchers.<Integer>emptyIterable()));
		assertThat(generateCollection, is(not(Matchers.<Integer>emptyIterable())));
	}
	
	/**
	 * We can check if collection/iterable has given item(s). Methods for that
	 * are {@link IsCollectionContaining#hasItem},
	 * {@link IsCollectionContaining#hasItems}
	 * {@link IsIterableContainingInOrder#contains},
	 * {@link IsIterableContainingInOrder#containsInAnyOrder}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void shouldEvaluateContentOfCollection()
	{
		Collection<Integer> generateCollection = EvenNumberGenerator.generateNumberCollection(3);
		
		assertThat(generateCollection, hasItem(2));
		assertThat(generateCollection, hasItems(2,4,6));

		assertThat(generateCollection, hasItem(equalTo(2)));
		assertThat(generateCollection, hasItems(equalTo(2), greaterThan(3), lessThan(7)));
		
		// iterable
		assertThat(generateCollection, contains(2,4,6));
		assertThat(generateCollection, containsInAnyOrder(2,6,4));
		
		assertThat(generateCollection, contains(equalTo(2), greaterThan(3), lessThan(7)));
		assertThat(generateCollection, containsInAnyOrder(equalTo(2), greaterThan(3), lessThan(7)));
	}
	
	/**
	 * We can evaluate size of collection/iterable with
	 * {@link IsCollectionWithSize#hasSize}
	 * {@link IsIterableWithSize#iterableWithSize}
	 */
	@Test
	public void shouldEvaluateCollectionSize()
	{
		Collection<Integer> generateCollection = EvenNumberGenerator.generateNumberCollection(3);
		
		// collection
		assertThat(generateCollection, hasSize(3));
		assertThat(generateCollection, hasSize(greaterThanOrEqualTo(1)));
		
		// iterable
		assertThat(generateCollection, IsIterableWithSize.<Integer>iterableWithSize(3));
		assertThat(generateCollection, IsIterableWithSize.<Integer>iterableWithSize(equalTo(3)));
	}
}
