package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.hamcrest.core.CombinableMatcher;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.math.Multiplier;

/**
 * Hamcrest allows us to construct more complex assertion where we can
 * link part of assertion with operators like and/or, we can specify that
 * only one from many assertion should be fulfilled, etc.
 */
public class MatchersAggregation 
{
	/**
	 * <p>
	 * If we want link two assertion with AND operator we need to use
	 * {@link CombinableMatcher#and} if we need OR operator, we use
	 * {@link CombinableMatcher#or}. If we want that many assertion has to be
	 * evaluate to true, we use {@link Matchers#allOf}, if we want only one from
	 * group of assertions to be evaluated to true, we use
	 * {@link Matchers#anyOf}
	 * </p>
	 * <p>
	 * We have two methods: {@link Matchers#both} and {@link Matchers#either}
	 * which are syntax sugar and allow us to build more fluent expressions with
	 * two assertions
	 * </p>
	 */
	@Test
	public void shouldComputeCorrectMultiplicationResult()
	{
		// given
		double four = 4;
		double eight = 8;
		
		// when
		double six = Multiplier.multipy(2, 3);

		// then
		assertThat(six, is(both(greaterThan(four)).and(lessThan(eight))));
		assertThat(six, is(either(greaterThan(four)).or(greaterThan(eight))));
		
		assertThat(six, allOf(greaterThan(four), lessThan(eight)));
		assertThat(six, anyOf(greaterThan(four), greaterThan(eight)));
	}
}
