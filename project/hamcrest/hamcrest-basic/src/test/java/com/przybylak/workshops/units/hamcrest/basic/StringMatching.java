package com.przybylak.workshops.units.hamcrest.basic;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.text.IsEmptyString.*;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.text.StringContainsInOrder.stringContainsInOrder;

import org.hamcrest.core.IsEqual;
import org.hamcrest.core.StringEndsWith;
import org.hamcrest.core.StringStartsWith;
import org.hamcrest.object.HasToString;
import org.hamcrest.text.IsEmptyString;
import org.hamcrest.text.IsEqualIgnoringCase;
import org.hamcrest.text.IsEqualIgnoringWhiteSpace;
import org.hamcrest.text.StringContainsInOrder;
import org.junit.Test;

import com.przybylak.workshops.units.hamcrest.person.Person;

/**
 * <p>
 * Hamcrest allows us to evaluate strings:
 * </p>
 * <ul>
 * <li>check if string starts with / ends with given sub string</li>
 * <li>check if string is empty (or null)</li>
 * <li>check if strings are equal</li>
 * <li>check if object has proper toString() method</li>
 * <li>check if string contains substrings in given order</li>
 * </ul>
 */
public class StringMatching 
{
	
	/**
	 * {@link StringStartsWith#startsWith} method allows us to evaluate
	 * beginning of the string
	 */
	@Test
	public void shouldCheckIfStringsStartsWithGivenString()
	{
		Person firstPerson = new Person("John", "Doe", 37);
		
		String firstPersonName = firstPerson.toString();
		
		assertThat(firstPersonName, startsWith("John"));
	}
	
	
	/**
	 * {@link StringEndsWith#endsWith} method allows us to evaluate
	 * end of given string
	 */
	@Test
	public void shouldCheckIfStringsEndsWithGivenString()
	{
		Person firstPerson = new Person("John", "Doe", 37);
		
		String firstPersonName = firstPerson.toString();
		
		assertThat(firstPersonName, endsWith("Doe"));
	}
	
	/**
	 * There are two methods that allows us to check if string is empty:
	 * {@link IsEmptyString#isEmptyString()} and
	 * {@link IsEmptyString#isEmptyOrNullString()}
	 */
	@Test
	public void shouldCheckIfStringIsEmpty()
	{
		Person firstPerson = new Person(new String(), new String(), 31);
		Person secondPerson = new Person(null, null, 21);
		
		String firstPersonName = firstPerson.getName();
		String secondPersonName = secondPerson.getName();
		
		assertThat(firstPersonName, isEmptyString());
		assertThat(secondPersonName, isEmptyOrNullString());
	}
	
	/**
	 * We can check equality of strings with regular {@link IsEqual#equalTo}
	 * method but also we have two additional methods:
	 * {@link IsEqualIgnoringCase#equalToIgnoringCase} and
	 * {@link IsEqualIgnoringWhiteSpace#equalToIgnoringWhiteSpace}
	 */
	@Test
	public void shouldCheckEqualityOfString()
	{
		Person firstPerson = new Person("John", "Doe", 37);
		
		String firstPersonName = firstPerson.toString();
		
		assertThat(firstPersonName, is(equalTo("John Doe")));
		assertThat(firstPersonName, is(equalToIgnoringCase("JoHn DOe")));
		assertThat(firstPersonName, is(equalToIgnoringWhiteSpace("    John Doe   ")));
	}
	
	/**
	 * Hamcrest allows us to check if given object's toString() method fulfill
	 * given requirement we check that with {@link HasToString#hasToString}
	 */
	@Test
	public void shouldReturnCorrectFirstNameAndLastName()
	{
		// given
		Person firstPerson = new Person("John", "Doe", 37);
		
		// then
		assertThat(firstPerson, hasToString("John Doe"));
		assertThat(firstPerson, hasToString(startsWith("John")));
		assertThat(firstPerson, hasToString(endsWith("Doe")));
	}
	
	/**
	 * {@link StringContainsInOrder#stringContainsInOrder} allows us to check if
	 * given string contains list of substring in given order
	 */
	@Test
	public void shouldCheckSubstrings()
	{
		Person person = new Person("John", "Doe", 37);
		
		List<String> subs = new ArrayList<String>();
		subs.add("Jo");
		subs.add("hn");
		subs.add("Doe");
		
		assertThat(person.toString(), stringContainsInOrder(subs));
	}
}
