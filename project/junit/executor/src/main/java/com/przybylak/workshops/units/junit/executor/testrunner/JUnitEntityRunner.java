package com.przybylak.workshops.units.junit.executor.testrunner;

import com.przybylak.workshops.units.junit.executor.ExecutionResult;

public interface JUnitEntityRunner 
{
	public ExecutionResult execute(StringBuilder output);
}
