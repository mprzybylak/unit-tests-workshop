package com.przybylak.workshops.units.junit.executor;

import java.lang.reflect.Method;

import org.junit.Test;
import org.junit.runner.Description;

import com.przybylak.workshops.units.junit.basic.GettingStarted;
import com.przybylak.workshops.units.junit.executor.testrunner.JUnitEntityRunnerFactory;
import com.przybylak.workshops.units.junit.executor.testrunner.runners.JUnitTestRunner;

public class JUnitTestExecutor 
{
	private Class<?>[] classes;
	private StringBuilder output = new StringBuilder();
	
	ExecutionResult executionResult = new ExecutionResult();
	
	public JUnitTestExecutor(Class<?> ... classes) 
	{
		this.classes = classes;
	}
	
	public void execute()
	{
		prepareTestHeader();
		prepareIntroduction();
		executeTests();
		prepareSummary();
		
		System.out.println(output.toString());
	}
	
	private void prepareTestHeader()
	{
		output.append("[ JUNIT RUNNER ]\n\n");
		output.append("Custom tests runner\n\n");
	}
	
	
	private void prepareIntroduction() 
	{
		output.append("[ Introduction ]\n\n");
		output.append("Following test classes' methods will be run:\n\n");
		
		for(Class<?> clazz : classes)
		{
			Description description = Description.createSuiteDescription(clazz);
			output.append("+ " + description + ":\n");
			for(Method method : clazz.getMethods())
			{
				if(method.getAnnotation(Test.class) != null)
				{
					output.append("\t- " + method.getName() + "\n");
				}
			}
		}
		output.append("\n");
	}
	
	private void executeTests()
	{
		output.append("[ Runnning Tests... ]\n\n");
		for(Class<?> clazz : classes)
		{
			JUnitTestRunner runner = JUnitEntityRunnerFactory.getInstance(clazz);
			ExecutionResult executionResult = runner.execute(output);
			this.executionResult.add(executionResult);
		}
	}
	
	private void executeTest(Class<?> clazz) 
	{
		
		
	}
	
	private void prepareSummary()
	{
		int testClassesCount = executionResult.getClassPassCount() + executionResult.getClassFailCount();
		int allTestCount = executionResult.getMethodPassCount() + executionResult.getMethodFailCount() + executionResult.getMethodIgnoreCount();
		
		int failedClassesCount = executionResult.getClassFailCount();
		
		int failedTestsCount = executionResult.getMethodFailCount();
		
		output.append("\nSummary:\n");
		output.append("\t- Class executed: " +  testClassesCount + "\n");
		output.append("\t -Class passed: " + executionResult.getClassPassCount() + "\n");
		output.append("\t- Class failed: " + failedClassesCount + "\n");
		output.append("\n");
		
		output.append("\t- Tests executed: " + allTestCount + "\n");
		output.append("\t- Tests passed:" + executionResult.getMethodPassCount() + "\n");
		output.append("\t- Tests failed: " + failedClassesCount + "\n");
		output.append("\t- Tests ignored: " + executionResult.getMethodIgnoreCount() + "\n");
		
		if(failedClassesCount == 0)
		{
			output.append("\nRESULT: Tests passed!");
		}
		else
		{
			double percent = ((double)failedClassesCount / (double)testClassesCount) * 100;
			double testPercent = ((double)failedTestsCount/(double)allTestCount) * 100;
			
			output.append("\nRESULT: Test failed! " + percent +"% of classes failed (" + testPercent + "% of tests)" );
		}
		
	}
	
	public static void main(String[] args) 
	{
		JUnitTestExecutor runner = new JUnitTestExecutor(GettingStarted.class);
		runner.execute();
		
//		Result result = JUnitCore.runClasses(MathematicTest.class);
//		
//		for(Failure failure : result.getFailures())
//		{
//			Description description = failure.getDescription();
//			Collection<Annotation> annotations = description.getAnnotations();
//			
//			StringBuilder sb = new StringBuilder();
//			sb.append("TEST FAILURE\n");
//			sb.append("------------\n\n");
//			sb.append("Failing test in class: \t" + description.getClassName() + "\n");
//			sb.append("Failing test method: \t" + description.getMethodName() + "\n");
//			
//			sb.append("\nTest class has following children (if any):\n");
//			for(Description child : description.getChildren())
//			{
//				sb.append("\t* " + child.getDisplayName());
//			}
//			
//			sb.append("\nTest method has following annotation(s) (if any):\n");
//			for(Annotation annotation : description.getAnnotations())
//			{
//				sb.append(" \t* " + annotation.toString() + "\n"); 
//			}
//			
//			System.out.println(sb.toString());
			
		
		
			
//			System.out.println(failure.toString());
//			System.out.println(failure.getMessage());
//			System.out.println(failure.getTestHeader());
//			System.out.println(failure.getTrace());
//			System.out.println(failure.getDescription());
//			System.out.println(failure.getException());
//		}
	}
}
