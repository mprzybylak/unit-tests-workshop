package com.przybylak.workshops.units.junit.executor.testrunner;

import org.junit.runner.Description;

import com.przybylak.workshops.units.junit.executor.testrunner.runners.JUnitTestRunner;

public class JUnitEntityRunnerFactory 
{
	public static JUnitTestRunner getInstance(Class<?> testClass)
	{
		JUnitTestRunner runner = null;
		Description description = Description.createSuiteDescription(testClass);
		
		if(description.isTest())
		{
			runner = new JUnitTestRunner(testClass);
		}
		
		return runner;
	}
}
