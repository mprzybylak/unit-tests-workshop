package com.przybylak.workshops.units.junit.executor.testrunner.runners;

import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import com.przybylak.workshops.units.junit.executor.ExecutionResult;
import com.przybylak.workshops.units.junit.executor.testrunner.JUnitEntityRunner;

public class JUnitTestRunner implements JUnitEntityRunner 
{
	Class<?> testClass;
	
	public JUnitTestRunner(Class<?> testClass) 
	{
		this.testClass = testClass;
	}

	@Override
	public ExecutionResult execute(StringBuilder output) 
	{
		
		Description description = Description.createSuiteDescription(testClass);
		output.append("Test class:\n" + description);
		
		Result result = JUnitCore.runClasses(testClass);

		ExecutionResult executionResult = ExecutionResult.createFromTestResult(result);
		
		if(result.getFailureCount() == 0)
		{
			output.append(" - SUCCESS!\n");
		}
		else
		{
			output.append((" - FAILURE!\n"));
			result.getFailures();
		}
		
		return executionResult;
	}

}
