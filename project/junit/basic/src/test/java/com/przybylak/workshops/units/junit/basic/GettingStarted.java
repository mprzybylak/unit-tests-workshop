package com.przybylak.workshops.units.junit.basic;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.przybylak.workshops.units.junit.math.Mathematic;

/**
 * <p>This class shows how to use junit in general</p>
 * <p>All tests need @Test annotation.</p>
 * <p>Code that should be reused in all test methods should be put inside methods
 * annotated with @Before (for example - some initializations) and @After (e.g. cleaning)</p>
 * <p>Code that should run only once should be put inside methods annotated with @BeforeClass 
 * and @AfterClass</p>
 */
@SuppressWarnings("unused")
public class GettingStarted 
{

	private Mathematic math;
	private static int[][] multiplyData;
	private static int[][] divisionData;
	
	/**
	 * Method annotated with {@link BeforeClass} will be run once before all
	 * tests. Methods with this annotation from super classes will run before
	 * methods from current class
	 */
	@BeforeClass //
	public static void setUpOnce()
	{
		// there are better ways to create test data
		multiplyData = new int[][]{{2,2,4}, {2,4,8}, {4,4,16}};
		divisionData = new int[][]{{2,2,1}, {4,2,2}, {8,2,4}};
	}
	
	/**
	 * Method annotated with {@link Before} will run before every test method.
	 * Methods with this annotation from super classes will run before methods
	 * from current class
	 */
	@Before //
	public void setUp()
	{
		math = new Mathematic();
	}
	
	/**
	 * Test methods should be annotated like this
	 */
	@Test 
	public void shouldReturnCorrectMultiplyResult()
	{
		for(int i = 0; i < multiplyData.length; ++i)
		{
			math.multiply(multiplyData[i][0],multiplyData[i][1]);
			int result = math.getResult();
		
			// basic junit assertion - test for equals
			assertEquals(multiplyData[i][2], result);
		}
	}
	
	@Test
	public void shouldReturnCorrectDivisionResult()
	{
		for(int i = 0; i < divisionData.length; ++i)
		{
			math.division(divisionData[i][0], divisionData[i][1]);
			int result = math.getResult();
		
			// another basic junit assertion - test if condition returns true
			assertTrue(result != divisionData[i][2] + 1);
		}
	}
	
	/**
	 * When we except exception - we use expected=Exception.class
	 */
	@Test(expected=ArithmeticException.class) 
	public void shouldThrowArithmeticExceptionOnDivisionByZero()
	{
		math.division(2, 0);
		int result = math.getResult();
		
		// this method will force failure
		fail();
	}
	
	/**
	 * We can specify timeout (in ms) for test methods. Test will fail on
	 * timeout.
	 */
	@Test(timeout=10)
	public void shouldFinishBeforeTimeout()
	{
		math.multiply(1, 1);
		int result = math.getResult();
		
		assertEquals(1, result);
	}
	
	/**
	 * Method annotated with {@link After} will run after every test method.
	 * Execution of method is guaranteed. Method with this annotation from super
	 * classes will also be executed (when methods from current class will
	 * finish).
	 */
	@After 
	public void tearDown()
	{
		math = null;
	}
	
	/**
	 * Static methods annotated with {@link AfterClass} will run once after all
	 * test methods. Execution of method is guaranteed. Methods with this
	 * annotation from super classes will also be executed (after current
	 * class).
	 */
	@AfterClass 
	public static void tearDownOnce()
	{
		multiplyData = null;
		divisionData = null;
	}
	
}
