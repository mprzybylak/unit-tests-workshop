package com.przybylak.workshops.units.junit.basic;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.przybylak.workshops.units.junit.math.Sum;

@RunWith(Parameterized.class)
public class ParametrizedTestsWithVaragrs 
{

	@Parameters
	public static Collection<Object[]> valuesToAdd()
	{
		// if we want to simulate varargs we have to do trick and pass array
		return Arrays.asList(new Object[][] {
				{1,  new int[]{1,0}},
				{3,  new int[]{1,1,1}},
				{13, new int[]{1,2,4,6}},
				{10, new int[]{1,3,6}},
				{29, new int[]{1,3,6,9,10}},
				{1,  new int[]{1}}
		});
	}
	private int[] inputs;
	private int expected;
	
	/**
	 *  from java points of view varargs and array passed as a last argument to methods
	 *  behave the same. Since junit can't pass varargs to constructor - we have to pass
	 *  parameters as an array
	 */
	public ParametrizedTestsWithVaragrs(int expected, int[] inputs) 
	{
		this.expected = expected;
		this.inputs = inputs;
	}
	
	@Test
	public void shouldSumInput()
	{
		// when
		int sum = Sum.sum(inputs);

		// then
		assertEquals(expected, sum);
	}
}
