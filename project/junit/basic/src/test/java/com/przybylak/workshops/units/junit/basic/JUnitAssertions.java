package com.przybylak.workshops.units.junit.basic;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.przybylak.workshops.units.junit.logical.Xor;
import com.przybylak.workshops.units.junit.math.EvenNumbersFilter;
import com.przybylak.workshops.units.junit.money.Salary;

/**
 * This class shows basic sets of assertions from JUnit
 */
public class JUnitAssertions 
{
	private int[] INT_EVEN_NUMBERS = {2, 4, 6, 8};
	private double[] DOUBLE_EVEN_NUMBERS = {2.1, 4.1, 6.1, 8.1};

	private static EvenNumbersFilter filter;
	
	@BeforeClass
	public static void setUpOnce()
	{
		filter = new EvenNumbersFilter();
	}
	
	/**
	 * {@link Assert#assertArrayEquals} allows to check if two arrays has equal
	 * content.
	 */
	@Test
	public void shouldGetOnlyEvenNumbers()
	{
		int[] ints = {1,2,3,4,5,6,7,8};
		int[] intsResult = filter.filter(ints);
		
		// assertArrayEquals comes with many flavors - for ints, doubles, Objects, etc. 
		assertArrayEquals(INT_EVEN_NUMBERS, intsResult);
	}
	
	@Test
	public void shouldGetOnlyEvenNumbersForDoublesWithDelta()
	{
		double[] doubles = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};
		double[] doublesResult = filter.filterDoubles(doubles);

		// for floats and doubles we can specify delta
		assertArrayEquals(DOUBLE_EVEN_NUMBERS, doublesResult, 0.2);
	}
	
	/**
	 * {@link Assert#assertEquals} allows to check if two objects are meaningful equal
	 */
	@Test
	public void shouldComputeProperSalary()
	{
		Salary salary = new Salary(1000);
		Salary raise = new Salary(500);
		Salary expected = new Salary(1500);
		
		salary.raise(raise);
		
		// assertEquals comes in many different flavors (for ints, doubles, objects, etc.)
		assertEquals(salary, expected);
	}
	
	@Test
	public void shouldComputeProperFloatSalary()
	{
		Salary salary = new Salary(1000);
		Salary raise = new Salary(500);
		Salary expected = new Salary(1500);
		
		salary.raise(raise);
		
		// for floats we can specify delta
		assertEquals(salary.getFloatValue(), expected.getFloatValue(), 0.1);
	}
	
	/**
	 * {@link Assert#assertTrue} and {@link Assert#assertFalse} allows to check
	 * if condition is true/false
	 */
	@Test
	public void shouldComputeProperXorValue()
	{
		boolean resultTrue = Xor.compute(false, true);
		boolean resultFalse = Xor.compute(false, false);
		
		// checks if condition is true
		assertTrue(resultTrue);
		
		// checks if condition is false
		assertFalse(resultFalse);
	}
	
	/**
	 * {@link Assert#assertNotNull} and {@link Assert#assertNull} allows to
	 * check if given object exists (!= null) or not
	 */
	@Test
	public void shouldReturnStringWithSalaryValue()
	{
		Salary salary = new Salary(3000);
		Salary salary2 = null;
		
		// checks if object is not null
		assertNotNull(salary.toString());
		
		// checks if object is null
		assertNull(salary2);
	}
	
	/**
	 * {@link Assert#assertSame} and {@link Assert#assertNotSame} allows to
	 * check if two references point to the same object or not
	 */
	@Test
	public void shouldCheckSalaryInstance()
	{
		Salary first = Salary.getNewInstance();
		Salary sec = Salary.getNewInstance();
		Salary third = sec;
		
		// checks if two references point to different objects
		assertNotSame(first, sec);
		
		// checks if two references point to the same object
		assertSame(sec, third);
	}
	
	/**
	 * All JUnit assertion has overloaded version with String as a first
	 * argument. This string allows us to provide message which will be returned
	 * to us when test fail.
	 */
	@Test
	public void shouldAlwaysPass()
	{
		// if test fail, we wil see "Impossible!" message.
		assertTrue("Impossible!", true);
	}
}
