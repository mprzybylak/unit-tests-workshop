package com.przybylak.workshops.units.junit.basic;

import org.junit.Assume;
import org.junit.Test;
import static org.junit.Assume.*;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

/**
 * Assumption mechanism allows to conditionally "ignore" test when there is no sense to run it.
 * For example we need to test values from database, but database engine is off, so we can't connect.
 * Normally test will fail in such case but in fact there is no faliure in source code, so we can
 * stop execution of given test and move on. 
 */
public class JUnitAssumptions 
{
	/**
	 * {@link Assume#assumeNoException(Throwable)} allows us to check if there was
	 * exception
	 */
	@Test
	public void exceptionAssumption()
	{
		try
		{
			assertTrue(true);
		}
		catch(Exception e)
		{
			assumeNoException(e);
		}
	}
	
	/**
	 * {@link Assume#assumeNotNull(Object...)} allows us to check if object is null
	 */
	@Test
	public void notNullAssumption()
	{
		Object obj = new Object();
		
		assumeNotNull(obj);
		assertTrue(true);
	}
	
	/**
	 * {@link Assume#assumeThat(Object, org.hamcrest.Matcher)} allows us to check condition
	 * in "hamcrest way"
	 */
	@Test
	public void hamcrestAssumption()
	{
		int i = 5;
		assumeThat(i, is(equalTo(5)));
		assertTrue(true); 
	}
	
	/**
	 * {@link Assume#assumeTrue(boolean)} allows us to check if given expression
	 * evaluates to true
	 */
	@Test
	public void trueAssumption()
	{
		assumeTrue(true);
		assertTrue(true);
	}
}
