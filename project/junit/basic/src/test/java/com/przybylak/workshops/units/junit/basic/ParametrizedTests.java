package com.przybylak.workshops.units.junit.basic;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.przybylak.workshops.units.math.Multiplier;

/**
 * JUnit Allows us to run the same tests with different input data. In order to
 * do that we need run us class with {@link Parametrized} class. Next we need
 * static method which provide data for tests. This method has to return
 * Collection with Parameters and has to be annotated with {@link Parameters}
 * annotation. Finally we need to create constructor which will get values in
 * order given in provider method. We have to store those parameters and we can
 * use them in actual tests
 */
@RunWith(Parameterized.class) // this annotation is mandatory in parametrized tests
public class ParametrizedTests 
{
	private int x;
	private int y;
	private int expected;
	
	/**
	 * This is provider method. It has to be static, it has to return Collection
	 * and it has to be annotated with {@link Parameters} annotation
	 */
	@Parameters
	public static Collection<Object[]> parameters()
	{
		return Arrays.asList(new Object[][]{
				{1, 1, 1},
				{2, 4, 8},
				{0, 4, 0},
				{0, -4, 0},
				{-1, 2, -2},
		});
	}
	
	/**
	 * Constructor takes an arguments from provider method we can store it for
	 * actual tests
	 */
	public ParametrizedTests(int x, int y, int expected) 
	{
		this.x = x;
		this.y = y;
		this.expected = expected;
	}
	
	/**
	 * In actual test we can use arguments passed by constructor
	 */
	@Test
	public void shouldComputeProperMultiplyResult()
	{
		// when
		int result = Multiplier.multipyInt(x, y);
		
		// then
		assertEquals(result, expected);
	}
}
