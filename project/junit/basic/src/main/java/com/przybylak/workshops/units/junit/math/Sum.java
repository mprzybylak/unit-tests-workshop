package com.przybylak.workshops.units.junit.math;

public class Sum 
{
	public static int sum(int ... inputs)
	{
		int sum = 0;
		for(int input : inputs)
		{
			sum += input;
		}
		return sum;
	}
}
