package com.przybylak.workshops.units.junit.money;

public class Salary 
{
	private int value;
	
	public Salary(int value) 
	{
		this.setValue(value);
	}
	
	public static Salary getNewInstance()
	{
		return new Salary(0);
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(!(obj instanceof Salary))
		{
			return false;
		}
		
		if(this == obj)
		{
			return true;
		}
		
		return (this.getValue() == ((Salary)obj).getValue());
	}

	public int getValue() 
	{
		return value;
	}
	
	public float getFloatValue()
	{
		return (float)value;
	}

	public void setValue(int value) 
	{
		this.value = value;
	}
	
	public void raise(Salary raiseValue)
	{
		value += raiseValue.getValue();
	}
	
	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append(value);
		sb.append("$");
		return sb.toString();
	}
}
