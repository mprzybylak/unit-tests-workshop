package com.przybylak.workshops.units.junit.logical;

public class Xor 
{
	public static boolean compute(boolean p, boolean q)
	{
		return (p & !q) || (!p & q);
	}
}
