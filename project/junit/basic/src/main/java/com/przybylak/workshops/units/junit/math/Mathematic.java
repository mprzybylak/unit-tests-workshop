package com.przybylak.workshops.units.junit.math;

public class Mathematic 
{

	int result = 0;
	
	public int getResult()
	{
		return result;
	}
	
	public void multiply(int i, int j) 
	{
		result =  i * j;
	}
	
	public void division(int i, int j)
	{
		result = i / j;
	}

}
