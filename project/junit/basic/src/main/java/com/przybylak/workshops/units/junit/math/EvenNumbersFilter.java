package com.przybylak.workshops.units.junit.math;

import java.util.LinkedList;
import java.util.List;

public class EvenNumbersFilter 
{

	public int[] filter(int[] numbers) 
	{
		List<Integer> temp = new LinkedList<Integer>();
		
		for(Integer number : numbers)
		{
			if(number % 2 == 0)
			{
				temp.add(number);
			}
		}
		
		int[] res = new int[temp.size()];
		
		for(int i = 0; i < res.length; ++i)
		{
			res[i] = temp.get(i);
		}
		
		return res;
	}
	
	public double[] filterDoubles(double[] numbers) 
	{
		List<Double> temp = new LinkedList<Double>();
		
		for(Double number : numbers)
		{
			if(number % 2 == 0)
			{
				temp.add(number);
			}
		}
		
		double[] res = new double[temp.size()];
		
		for(int i = 0; i < res.length; ++i)
		{
			res[i] = temp.get(i);
		}
		
		return res;
	}

}
