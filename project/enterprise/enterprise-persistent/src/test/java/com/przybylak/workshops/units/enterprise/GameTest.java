package com.przybylak.workshops.units.enterprise;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class GameTest 
{
	@Deployment
	public static Archive<?> createDeployment()
	{
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackage(Game.class.getPackage())
				.addAsResource("test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	private static final String[] TITLES = { "t1", "t2", "t3" };
	
	@PersistenceContext
	EntityManager em;
	
	@Inject
	UserTransaction utx;
	
	@Before
	public void preparePersistenceTest() throws Exception {
	    clearData();
	    insertData();
	    startTransaction();
	}
	
	private void clearData() throws Exception 
	{
	    utx.begin();
	    em.joinTransaction();
	    System.out.println("Dumping old records...");
	    em.createQuery("delete from Game").executeUpdate();
	    utx.commit();
	}
	
	private void insertData() throws Exception 
	{
	    utx.begin();
	    em.joinTransaction();
	    System.out.println("Inserting records...");
	    for (String title : TITLES) {
	        Game game = new Game(title);
	        em.persist(game);
	    }
	    utx.commit();
	    // clear the persistence context (first-level cache)
	    em.clear();
	}
	
	private void startTransaction() throws Exception 
	{
	    utx.begin();
	    em.joinTransaction();
	}
	
	@After
	public void commitTransaction() throws Exception {
	    utx.commit();
	}
	
	@Test
	public void shouldFindAllGamessUsingJpqlQuery()
	{
		// given
		String query = "select g from Game g order by g.id";
		
		// when
		List<Game> games = em.createQuery(query, Game.class).getResultList();

		// then
		assertContainsAllGames(games);
	}
	
	private static void assertContainsAllGames(Collection<Game> retrievedGames) {
	    Assert.assertEquals(TITLES.length, retrievedGames.size());
	    final Set<String> retrievedGameTitles = new HashSet<String>();
	    for (Game game : retrievedGames) {
	        System.out.println("* " + game);
	        retrievedGameTitles.add(game.getTitle());
	    }
	    Assert.assertTrue(retrievedGameTitles.containsAll(Arrays.asList(TITLES)));
	}
}
