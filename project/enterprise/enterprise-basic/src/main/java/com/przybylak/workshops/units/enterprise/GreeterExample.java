package com.przybylak.workshops.units.enterprise;

import java.io.PrintStream;

import javax.inject.Inject;

public class GreeterExample 
{
	private PhraseBuilder phraseBuilder;

	@Inject
	public GreeterExample(PhraseBuilder phraseBuilder) 
	{
		this.phraseBuilder = phraseBuilder;
	}
	
	 public void greet(PrintStream to, String name) 
	 {
		 to.println(createGreeting(name));
	 }

	 public String createGreeting(String name) 
	 {
		 return phraseBuilder.buildPhrase("hello", name);
	 }
}
